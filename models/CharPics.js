module.exports = (sequelize, DataTypes) => {
    return sequelize.define('charpics', {
        key_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        server: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        user: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        picurl: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        charinfo: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {
        timestamps: true,
    });
};