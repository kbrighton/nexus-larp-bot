module.exports = (sequelize, DataTypes) => {
    return sequelize.define('initdodge', {
        key_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        channel: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        init: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
        },
        dodge: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
        },
    }, {
        timestamps: true,
    });
};