const an = require("./anextmodules");
exports.myDateTime = function () {  //Returns the current time, in case you need that.
    return Date();
};

function ARandomNumber(maxr) {  //Returns a random number between 1 and maxr
    //return Math.floor(Math.random() * (maxr - 1 + 1)) + 1
    return Math.floor(Math.random() * Math.floor(maxr)) + 1;
}

exports.GetRandomNumber = function (maxr) {  //Export function for ARandomNumber
    return ARandomNumber(maxr);
};

exports.getUserFromMention = function (mention) {
    // The id is the first and only match found by the RegEx.
    const matches = mention.match(/^<@!?(\d+)>$/);

    // If supplied variable was not a mention, matches will be null instead of an array.
    if (!matches) return;

    return matches[0];

}


function ACritFailState(message, MSG) {  //Returns the message that the user should check for crit fail.  (Mostly here for easy changing in one place if necessary.)
    message.reply(MSG + ' Please make a critical failure check using \`!CritFail (optional "Rock/Paper/Scissors")\`');

    /*													//This chunk would let players choose an emoji to pick R P or S, but it doesn't work great.
        user1 = message.author;
        message.reply(MSG + `Please pick an emoji below for your CritFail test.`).then(sentMSG => {
            sentMSG.react('696521932773785651');
            sentMSG.react('696521932836962374');
            sentMSG.react('696521932614533121');

            const filter = (reaction, user) => {
                console.log(`User.id = ${user.id} and User1.id = ${user1.id}`)
                return (user.id == user1.id); // && (reaction.emoji.name === '✅' || reaction.emoji.name === '❌');
            };

            const collector = sentMSG.createReactionCollector(filter, { maxUsers: 2, time: 10000, errors: ['time'] });

            collector.on('end', collected => {

                console.log(`Collected ${collected.size} items ` + collected.map(item => `${item.emoji.name}: ` + require('util').inspect(item)).join("\n") );  //s.map(user => `${user.username}`).join(" - ")
            });

        });

     */
}

exports.CritFailState = function (message, MSG) {  //Export function for ACritFailState
    ACritFailState(message, MSG);
};

function AThrowResult(ANumber, rGroup) {  //Give it the number of the throw (1-3) and a random group number, and it returns an emoji of Rock, Paper, or Scissors.
    const {RockEmoji1, PaperEmoji1, ScisEmoji1, RockEmoji2, PaperEmoji2, ScisEmoji2} = require('./global.json');

    switch (rGroup) {
        case 1:
        default:
            switch (ANumber) {
                case 1:
                    return RockEmoji1
                    break;
                case 2:
                    return PaperEmoji1
                    break;
                case 3:
                    return ScisEmoji1
                    break;
            }
            break;
        case 2:
            switch (ANumber) {
                case 1:
                    return RockEmoji2
                    break;
                case 2:
                    return PaperEmoji2
                    break;
                case 3:
                    return ScisEmoji2
                    break;
            }
            break;
    }
}


exports.ShowThrowResult = function (ANumber, rGroup) {
    return AThrowResult(ANumber, rGroup)
}

function AJustOneThrow(message) {
    var RN = ARandomNumber(3);
    var rGroup = ARandomNumber(5);
    var strThrown = AThrowResult(RN, rGroup);

//    if (PlayKCD) {
//        strThrown = an.KCDThrowResult(ANum);
//    }

    message.reply(` throws ${strThrown}`);
}

exports.JustOneThrow = function (message) {
    AJustOneThrow(message)
}

exports.ThrowRock = function (message) {
    var rGroup = ARandomNumber(5);
    var strThrown = AThrowResult(1, rGroup);
    message.reply(` throws ${strThrown}`);
}

exports.ThrowPaper = function (message) {
    var rGroup = ARandomNumber(5);
    var strThrown = AThrowResult(2, rGroup);
    message.reply(` throws ${strThrown}`);
}

exports.ThrowScissors = function (message) {
    var rGroup = ARandomNumber(5);
    var strThrown = AThrowResult(3, rGroup);
    message.reply(` throws ${strThrown}`);
}

function AWinner(ANum, BNum) {
    var rT = 'T'
    var rA = 'A'
    var rB = 'B'
    if (ANum == BNum) {
        return rT
    } else if (ANum == (BNum + 1)) {
        return rA
    } else if (BNum == (ANum + 1)) {
        return rB
    } else if (ANum == 1) {
        return rA
    } else if (BNum == 1) {
        return rB
    }
}

exports.Winner = function (ANum, BNum) {
    return AWinner(ANum, BNum)
}

function AOneMatch(message, AName, BName, MatchNum, post, kcd) {
    var ANum = ARandomNumber(3)
    var BNum = ARandomNumber(3)
    var rGroup = ARandomNumber(5)
    var Win = AWinner(ANum, BNum)
    var WinName
    if (Win == 'A') {
        WinName = '<@' + AName + '> wins this throw!'
    } else if (Win == 'B') {
        WinName = BName + ' wins this throw!'
    } else {
        WinName = 'It is a tie!'
    }
    var ResultString = 'Throw #' + MatchNum + ': <@' + AName + '> throws ' + AThrowResult(ANum, rGroup) + ' vs ' + BName + ' throws ' + AThrowResult(BNum, rGroup) + '! ' + WinName;

//    if (PlayKCD || kcd) {
//        ResultString = `Throw #${MatchNum}: ${AName} throws ${an.KCDThrowResult(ANum)} vs ${BName} throws ${an.KCDThrowResult(BNum)}! ${WinName}`
//    }

    if (post) {
        message.channel.send(ResultString);
    }

    return Win
}

exports.OneMatch = function (message, AName, BName, MatchNum, post, kcd) {
    return AOneMatch(message, AName, BName, MatchNum, post, kcd)
}

function ARpsRoundHigher(message, AName, ANum, BName, BNum, post) {
    var TotalThrows = (ANum - BNum)
    var MWin = 'T'
    var ThisThrow = 0
    var WinRound = 'F'

    do {
        ThisThrow = ThisThrow + 1
        MWin = AOneMatch(message, AName, BName, ThisThrow, post)
    } while ((MWin == 'B') & (ThisThrow < TotalThrows))

    if (MWin == 'B') {
        WinRound = 'C'
    } else {
        WinRound = 'P'
    }

    return WinRound
}

exports.RpsRoundHigher = function (message, AName, ANum, BName, BNum, post) {
    return ARpsRoundHigher(message, AName, ANum, BName, BNum, post)
}

function ARpsRoundLower(message, AName, ANum, BName, BNum, post) {
    var TotalThrows = (BNum - ANum)
    var MWin = 'T'
    var ThisThrow = 0
    var WinRound = 'F'

    do {
        ThisThrow = ThisThrow + 1
        MWin = AOneMatch(message, AName, BName, ThisThrow, post)
    } while ((MWin == 'A') & (ThisThrow < TotalThrows))

    if (MWin == 'A') {
        WinRound = 'P'
    } else {
        WinRound = 'F'
        if ((MWin == 'B') & (ThisThrow == 1)) {
            WinRound = 'C'
        }
    }

    return WinRound
}

exports.RpsRoundLower = function (message, AName, ANum, BName, BNum, post) {
    return ARpsRoundLower(message, AName, ANum, BName, BNum, post)
}

function ARpsRoundEqual(message, AName, ANum, BName, BNum, post) {
    var matchNo = 0;
    var Mwin = 'T';
    var AllWin = 'P';

    do {
        matchNo = matchNo + 1;
        Mwin = AOneMatch(message, AName, BName, matchNo, post);
    } while (Mwin == 'T');
    if (Mwin == 'A') {
        AllWin = 'P'
    } else {
        AllWin = 'F'
    }

    return AllWin
}

exports.RpsRoundEqual = function (message, AName, ANum, BName, BNum, post) {
    return ARpsRoundEqual(message, AName, ANum, BName, BNum, post)
}

exports.RpsDoIWin = function (message, ANum, BNum) {
    let Win = "T"
    if (ANum > BNum) {
        return ARpsRoundHigher(message, "A", ANum, "B", BNum, false)
    } else if (ANum < BNum) {
        return ARpsRoundLower(message, "A", ANum, "B", BNum, false)
    } else {
        return ARpsRoundEqual(message, "A", ANum, "B", BNum, false)
    }
    return Win;
}

function AExcelenceTest(message, AName, BName, ANumber, post) {
    var matchNo = 0;
    var Mwin = 'T';
    var AllWin = 'F';
    var BName2

    do {
        matchNo = matchNo + 1
        BName2 = BName + ' ' + matchNo

        if (post) {
            message.channel.send(`Excelence Test at a ${ANumber}, Round ${matchNo}`);
        }

        if (ANumber > matchNo) {
            AllWin = ARpsRoundHigher(message, AName, ANumber, BName2, matchNo, post)
        } else if (ANumber == matchNo) {
            AllWin = ARpsRoundEqual(message, AName, ANumber, BName2, matchNo, post)
        } else {
            AllWin = ARpsRoundLower(message, AName, ANumber, BName2, matchNo, post)
        }
    } while (AllWin == 'P')

    return (matchNo - 1)
}

exports.ExcelenceTest = function (message, AName, BName, ANumber, post) {
    return AExcelenceTest(message, AName, BName, ANumber, post)
}

function ACritFailTest(message, post) {
    var AName = message.author;
    var AllWin = ARpsRoundHigher(message, AName, 4, 'CritFail', 3, post);

    if (post) {
        if (AllWin == 'C') {
            message.reply('sorry, you **FAILED** your Critical Failure test‼️')
        } else {
            message.reply('you **PASSED** your Critical Failure test!')
        }
    }
    return AllWin;
}

exports.TestCritFail = function (message, post) {
    return ACritFailTest(message, post)
}

function APullAttachmentInfo(message) {
    var strReturn = 'Has an attachment';
    return strReturn;

}

exports.PullAttachmentInfo = function (message) {
    return APullAttachmentInfo(message)
}

exports.WelcomeMessage = function (message) {
    const data = [];

    const larpName = message.guild.name;

    data.push(`Welcome to the ${larpName} Online LARP.  I am the Nexus LARP Bot, here to help run your Online LARP experience.`);
    data.push('Use me to run your Rock-Paper-Scissors checks.  Type `!help` to get my list of commands at any time, and `!rules` to see a quick rundown of the LARP rules.');
    data.push('This Server is PG-13 so please watch your language, thank you.');

    return data;
}