//
//This file reads the database architecture into a local file as prep for reading/writing.
//To add a db model, copy the require line below, and change to your DB file name
//If you do make a change to a model, you can execute "node dbInit.js --force" or "node dbInit.js -f" to force sync your tables.
//It's important to note that this will empty out and remake your model tables.
//

const Sequelize = require('sequelize');

const sequelize = new Sequelize('database', 'username', 'password', {
    host: 'localhost',
    dialect: 'sqlite',
    logging: false,
    storage: 'database.sqlite'
});

require('./models/InitDodge')(sequelize, Sequelize);
require('./models/CharPics')(sequelize, Sequelize);

const force = process.argv.includes('--force') || process.argv.includes('-f');

sequelize.sync({force}).then(async () => {
    console.log('Database synced');
    sequelize.close();
}).catch(console.error);