import os
import re
import discord
from discord.ext import commands
import config
from dbObjects import InitDodge
from mymodules import dt
from discordlogs import dl
from discordlogsnew import dln
from anextmodules import an
from loggingsetup import ls

intents = discord.Intents.all()

bot = commands.Bot(command_prefix=config.prefix, intents=intents)

@bot.event
async def on_ready():
    print('Ready!')
    ls.readLogServersFile()
    await bot.change_presence(activity=discord.Game(name='Rock Paper Scissors'))

@bot.event
async def on_message(message):
    dl.messageLogging(message)
    if message.author.bot or not message.content.startswith(config.prefix):
        return

    args = message.content[len(config.prefix):].split()
    command_name = args.pop(0).lower()

    command = (bot.get_command(command_name) or
               next((cmd for cmd in bot.commands if command_name in cmd.aliases), None))
    if command is None:
        return

    if command.args and not args:
        reply = f"You didn't provide any arguments, {message.author}!"
        if command.usage:
            reply += "\nThe proper usage would be: `{config.prefix}{command_name} {command.usage}`"
        await message.channel.send(reply)
        return

    if (command.gmonly and not any(role.name in ['GMs', 'AGMs'] for role in message.author.roles)):
        await message.channel.send(f"I'm sorry {message.author}, I can't let you do that.")
        return

    ctx = await bot.get_context(message)
    await command.invoke(ctx)

for filename in os.listdir('./commands'):
    if filename.endswith('.py'):
        bot.load_extension(f'commands.{filename[:-3]}')

@bot.event
async def on_guild_join(guild):
    print(f'New guild joined: {guild.name}(id: {guild.id}). This guild has {guild.member_count} members!')
    Welcome = dt.WelcomeMessage()
    await guild.system_channel.send(Welcome)

@bot.event
async def on_message_edit(old, new):
    tempArray = ls.isLogGuild(old.guild.id)
    if tempArray != -1 and tempArray[3] == "1" and not old.author.bot and not old.content.startswith(config.prefix):
        dln.editLogging(new, old, tempArray)

@bot.event
async def on_message_delete(message):
    tempArray = ls.isLogGuild(message.guild.id)
    if tempArray != -1 and tempArray[3] == "1" and not message.author.bot and not message.content.startswith(config.prefix):
        dln.deleteLogging(message, tempArray)

@bot.event
async def on_raw_reaction_add(payload):
    # put your code here

bot.run(config.token)