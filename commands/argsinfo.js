//You can probably ignore this, I just used it to test a bunch of things about arguments to use in !rpsVS  ~Victor

module.exports = {
    name: 'argsinfo',
    description: 'ArgsInfo!',
    args: true,
    gmonly: false,
    usage: 'Add some words',
    execute(message, args) {

        const {RockEmoji, PaperEmoji, ScisEmoji} = require('../global.json');
        var dt = require('../mymodules');

//		message.channel.send('ArgsInfo command.');
        if (!args.length) {
            //return message.channel.send(`You didn't provide any arguments, ${message.author}!`);
        }

        var RN = dt.GetRandomNumber(10);
        var BString = "";
        var BNumber = -100;
        var BName = "";
        var i = 0;
        message.reply(`Arguments: ${args} ${RN}`);
        for (i = 1; i < args.length; i++) {

            console.log(args[i]);
            BString = args[i];
            BNumber = parseInt(BString);
            if (isNaN(BNumber)) {
                if (i == 1) {
                    BName = BString;
                } else {
                    BName = BName + ' ' + BString;
                }

            } else {
                break;
            }
        }

        if (isNaN(BNumber)) {
            message.reply('No number in args');
        } else {
            console.log('Out of loop; BName = ' + BName + '; BNumber = ' + BNumber);
        }
    },
};