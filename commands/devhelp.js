module.exports = {
    name: 'devhelp',  //name of command in lowercase
    description: 'List of commands someone programming the bot might want to know.',
    aliases: ['devcommands'],
    args: true,
    gmonly: false,  //make true if only GMs should be able to use this command

    execute(message, args) {
        var dt = require('../mymodules');

        const data = [];
        const {commands} = message.client;

        data.push('Developer Command List:');
        data.push('After changing a command use \`!reload <command name>\`');
        data.push('After adding a brand new command use \`!load <command name>\`');


        if (message.channel.type === 'dm') {
            return message.author.send(data)
        } else {


            //const channel = message.guild.channels.cache.get('698176250547798047');
            const channel = message.guild.channels.cache.find(channel => channel.name === "gm-bot-help");

            if (channel == null) {
                return message.reply(`There is no channel for me to send the commands to.  Please make a channel called "#gm-bot-help"`)
            }

            channel.send(data);


//			for (var value of message.guild.channels.cache.values()) {
//				console.log(value);
//			};


        }


    },
};