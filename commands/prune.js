module.exports = {
    name: 'prune',
    description: 'Delete a number of messages.  Do not count the "!prune" in number of messages to delete.\nDoes not work on messages over two weeks old.',
    aliases: ['trim', 'delete', 'purge'],
    args: true,
    gmonly: true,
    usage: '<number between 1 and 99>',
    execute(message, args) {
        var dt = require('../mymodules');

        console.log('In Prune');

        const amount = parseInt(args[0]) + 1;

        if (isNaN(amount)) {
            return message.reply('that doesn\'t seem to be a valid number.');
        } else if ((amount < 2) || (amount > 100)) {
            return message.reply('Please pick a number between 2 and 100');
        } else {
            message.channel.bulkDelete(amount);
        }


    },
};