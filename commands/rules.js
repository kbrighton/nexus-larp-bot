module.exports = {
    name: 'rules',
    description: 'Sends a private message to whoever asks for the rules.',
    aliases: ['commands'],

    args: false,
    gmonly: false,
    execute(message, args) {
        const data = [];
        const {commands} = message.client;

//		message.reply ('Testing rules.');

        data.push(`${message.guild.name} Rules:`);
        data.push('This Server is PG-13 so please watch your language, thank you.');
        data.push('This is still a work in progress.');
        data.push('The rules will be available by the time of the LARP.');

        return message.author.send(data, {split: true})
            .then(() => {
                if (message.channel.type === 'dm') return;
                message.reply('I\'ve sent you a DM with the LARP rules!');
            })
            .catch(error => {
                console.error(`Could not send help DM to ${message.author.tag}.\n`, error);
                message.reply('it seems like I can\'t DM you! Do you have DMs disabled?');
            });
    },
};