module.exports = {
    name: 'rpsettest',
    description: 'test rpset',
    aliases: ['ALIAS'],
    args: true,
    gmonly: false,
    usage: '[command name] + <args>',
    execute(message, args) {
        var dt = require('../mymodules');

//		message.reply(` WRITE EXCELENCE TEST FUNCTION `);

        var AName = message.author;
        var BName = 'Skill Level';
        var AString = args[0];
        var ANumber = parseInt(AString);
        var post = false;
        var ResultString = '';


        if (isNaN(ANumber) || (ANumber > 15)) {
            let reply = `I didn\'t understand that, ${message.author}!`
            reply += '\nThe proper usage would be: \`!rpsET # (Number of your skill level)\`';
            message.channel.send(reply);
            return
        }

        //message.channel.send(`Excelence Test: ${message.author} at a skill of ${ANumber}`);

        var i;
        for (i = 0; i < 100; i++) {

            var WinLevel = dt.ExcelenceTest(message, AName, BName, ANumber, post);

            if (WinLevel == 0) {
                ResultString += '0,';
            } else {
                ResultString += WinLevel + ',';
            }

        }

        message.channel.send(`Results of 100 tests:  ${ResultString}`);

    },
};