module.exports = {
    name: 'listchannelids',
    description: 'lists all channels and ids in the console',
    args: false,
    gmonly: true,

    execute(message, args) {
        var dt = require('../mymodules');

        console.log(`Guild name: ${message.guild.name} id: ${message.guild.id}`);

        for (var value of message.guild.channels.cache.values()) {
            console.log(`name: ${value.name} id: ${value.id} parent: ${value.parentID}`);
        }

    },
};