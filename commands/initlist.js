module.exports = {
    name: 'initlist',  //name of command in lowercase
    description: 'Displays a list of users in init order, and a list of doges',
    aliases: ['combatlist', 'list'],
    args: false,
    gmonly: true,  //make true if only GMs should be able to use this command

    execute(message, args) {
        (async () => {
            var dt = require('../mymodules');
            const {InitDodge} = require('../dbObjects');
            const {Op} = require('sequelize');

            const initList = await InitDodge.findAll({        					//Querries the DB, and returns a collection of
                where: {																//INIT results from this channel
                    channel: [message.channel.id.toString()]
                },
                order: [
                    ['init', 'DESC'],
                    ['name', 'ASC'],
                ],
                attributes: ['key_id', 'init', 'name',]
            },);
            if (!initList.length) return message.channel.send(`Initiative List Empty`);
            //console.log(initList);
            //return message.channel.send('Initiative Order:\n' + initList.map(item => `${item.key_id}: ${item.init}: ${item.name}`).join('\n'), { code: true });


            const initCounts = await InitDodge.findAll({                                 //Querries the DB, and returns a count of each init
                where: {
                    channel: [message.channel.id.toString()]
                },
                group: ['init'],
                order: [
                    ['init', 'DESC']
                ],
                attributes: ['key_id', 'init', [InitDodge.sequelize.fn('COUNT', InitDodge.sequelize.col('init')), 'initCount'],],
                raw: true,
            },);


            var iCount = 0;
            var oldInit = 0;															//Loops through the collection, to put multiple users with
            var MSG = "INITIATIVE LIST:";												//the same init on the same line (to save space).
            for (var i = 0, len = initList.length; i < len; i++) {
                if (initList[i].init == oldInit) {
                    MSG += ` -- `
                } else {
                    MSG += `\n${initCounts[iCount].init} (${initCounts[iCount].initCount}): `;
                    iCount += 1;
                }
                MSG += `${initList[i].init}: ${initList[i].name} `;
                oldInit = initList[i].init;
            }

            message.channel.send(MSG, {code: true}).then(sentMSG => {					//Sends the init list, and pins it to the channel
                sentMSG.pin();
            });

            const dodgeList = await InitDodge.findAll({        					//Querries the DB, and returns a collection of
                where: {																//DODGE results from this channel
                    channel: [message.channel.id.toString()]
                },
                order: [
                    ['name', 'ASC'],
                ],
                attributes: ['key_id', 'dodge', 'name',]
            },);

            MSG = "DODGE LIST:\n";
            for (var i = 0, len = dodgeList.length; i < len; i++) {
                MSG += `${dodgeList[i].name}: ${dodgeList[i].dodge} \n`;
            }

            message.channel.send(MSG, {code: true}).then(sentMSG => {					//Sends the dodge list, and pins it to the channel
                sentMSG.pin();
            });

        })();

    },
};


//const target = message.mentions.users.first() || message.author;
//const user = await Users.findOne({ where: { user_id: target.id } });
//const items = await user.getItems();

//if (!items.length) return message.channel.send(`${target.tag} has nothing!`);
//return message.channel.send(`${target.tag} currently has ${items.map(i => `${i.amount} ${i.item.name}`).join(', ')}`);
