module.exports = {
    name: 'raceupdate',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'Updates racers speed in the channel list so the bot can keep track of everyone\'s position',
    //aliases: ['ALIAS1', 'ALIAS2'],  //if no aliases remove this line
    args: true,     //does this function require any args?
    gmonly: false,  //make true if only GMs should be able to use this command
    usage: '<Current Speed>', //If there are args, please fill in this line with the proper order of arguments, if not, remove this line.

    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            let dt = require('../mymodules');
            const {InitDodge} = require('../dbObjects');  //These two lines only required if this command
            const {Op} = require('sequelize');           //references database objects.

            let Speed = parseInt(args.shift());

            if (isNaN(Speed)) {
                return message.reply(`I'm sorry, I didn't understand that.`)
            }

            let UseName = message.guild.member(message.author).displayName;

            const RaceEntry = await InitDodge.findOne({                             //Check if user already submitted for this channel
                where: {channel: message.channel.id.toString(), name: UseName},
            });

            if (RaceEntry) {                                                        //if user already submitted, update
                let Change = Speed - RaceEntry.init;
                RaceEntry.dodge = Change;
                RaceEntry.init = Speed;
                RaceEntry.save()
                    .then(message.reply(`Updated ${UseName} to Speed: ${Speed} Change: ${Change}`))
                message.delete();

            } else {

                message.reply(`I can't update you because you're not on the list of racers!`);
            }


            //code goes here


        })();  //use to close the async if you're using it.
    },
};