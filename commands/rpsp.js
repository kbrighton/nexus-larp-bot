module.exports = {
    name: 'rpsp',  //name of command in lowercase
    description: 'Throws paper',
    aliases: ['rspp'],
    args: false,
    gmonly: false,  //make true if only GMs should be able to use this command

    execute(message, args) {
        var dt = require('../mymodules');

        dt.ThrowPaper(message)


    },
};