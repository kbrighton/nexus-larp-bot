module.exports = {
    name: 'd10',  //name of command in lowercase
    description: 'Rolls 1d10',
    args: false,
    gmonly: false,

    execute(message, args) {
        var dt = require('../mymodules');

        return message.reply(`On 1d10 you rolled a ${dt.GetRandomNumber(10)}`);

    },
};