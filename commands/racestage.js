module.exports = {
    name: 'racestage',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'Use to set up each round of a race.  Bot will ask the player to input their change of speed, and current Pilot Vehicle',
    aliases: ['racestagesetup', 'racenewstage'],  //if no aliases remove this line
    args: false,     //does this function require any args?
    gmonly: true,  //make true if only GMs should be able to use this command
    //usage: '<arg1> <arg2>', //If there are args, please fill in this line with the proper order of arguments, if not, remove this line.

    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            let dt = require('../mymodules');
            const {InitDodge} = require('../dbObjects');  //These two lines only required if this command
            const {Op} = require('sequelize');           //references database objects.

            const Discord = require('discord.js');
            const raceEmbed = new Discord.MessageEmbed;

            raceEmbed
                .setColor('#ffff00')
                .setTitle(`Time for the next stage of the race!`)
                .setDescription(`Please set your speed for this stage.  Remember, the maximum* amount you can change your speed is 2.`)
                .addFields(
                    {name: `If you are Maintaining Speed:`, value: `Use \`!race m <Current Speed>\``},
                    {
                        name: `If you are Slowing Down:`,
                        value: `Use \`!race s <New Speed>\`\nDon't forget to add 1 to your Pilot Vehicle for the next turn.`
                    },
                    {
                        name: `If you are Speeding Up:`,
                        value: `Use \`!race <Pilot Vehicle> <New Speed>\`\nDon't forget to add any bonuses to your Pilot Vehicle.`
                    },
                )
                .setFooter(`*without enhancements`);


            message.channel.send(raceEmbed);
            message.delete();


        })();  //use to close the async if you're using it.
    },
};