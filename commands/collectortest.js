module.exports = {
    name: 'collectortest',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'test to see what I can do with emoji collectors',
    args: false,     //does this function require any args?
    gmonly: true,  //make true if only GMs should be able to use this command


    execute(message, args) {
//      (async () => {   //only use if code requires asnyc like .then or .await
        var dt = require('../mymodules');
//          const { InitDodge } = require('../dbObjects');  //These two lines only required if this command
//          const { Op } = require('sequelize');           //references database objects.

        message.react('✅');
        message.react('❌');
        message.react('‼️');

        const {botID} = require('../config.json');
        const filter = (reaction, user) => {
            return !(user.id == botID); //(reaction.emoji.name === '✅' || reaction.emoji.name === '❌') && !(user.id == botID);
        };

        const collector = message.createReactionCollector(filter, {maxUsers: 2, time: 15000, errors: ['time']});

        collector.on('collect', (reaction, user) => {
            reaction.users.remove(user);
            console.log(`Collected ${reaction.emoji.name} from ${user.tag}`);

        });

        collector.on('end', collected => {

            console.log(`Collected ${collected.size} items ` + collected.map(item => `${item.emoji.name}: ${item.message.author.username}: ` + require('util').inspect(item)).join("\n"));  //s.map(user => `${user.username}`).join(" - ")
        });


//		})();  //use to close the async if you're using it.
    },
};