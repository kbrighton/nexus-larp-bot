module.exports = {
    name: 'critfail',
    description: 'Test for Critical Failure',
    aliases: ['crit'],
    args: false,
    gmonly: false,
    usage: '(Optional: Rock Paper or Scissors)',

    execute(message, args) {

        var dt = require('../mymodules');

        if (!args.length) {											//If no argument, pick randomly
            dt.TestCritFail(message, true)
        } else {													//If there is an arg, use that as their RPS choice

            let re = /[rps]/i;										//Use RegEx to pull out the first R P or S
            const argsString = args.join(' ').toLowerCase();
            var RPS = argsString.match(re);

            if (RPS == null) {
                return message.reply(`You didn't type Rock Paper or Scissors!`)
            } else {
                RPS = RPS.toString()
            }

            //console.log(`RPS = ${RPS}`)

            switch (RPS) {
                case "r":
                    ANum = 1
                    break;
                case "p":
                    ANum = 2
                    break;
                case "s":
                    ANum = 3
                    break;

            }

            var AName = message.author;
            var BName = 'CritFail'
            var BNum = dt.GetRandomNumber(3)
            var rGroup = dt.GetRandomNumber(5)
            var Win = dt.Winner(ANum, BNum)
            var WinName = "";
            var Winner = "";
            if (Win == 'A') {
                WinName = '<@' + AName + '> wins this throw!'
                Winner = 'You **PASSED** your Critical Failure test!'
            } else if (Win == 'B') {
                WinName = BName + ' wins this throw!'
                Winner = 'Sorry, you **FAILED** your Critical Failure test‼️'
            } else {
                WinName = 'It is a tie!'
                Winner = 'You **PASSED** your Critical Failure test!'
            }

            const data = [];
            data.push('<@' + AName + '> throws ' + dt.ShowThrowResult(ANum, rGroup) + ' vs ' + BName + ' throws ' + dt.ShowThrowResult(BNum, rGroup) + '! ' + WinName);
            data.push(Winner);
            return message.channel.send(data);

        }

    },
};