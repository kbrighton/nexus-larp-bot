module.exports = {
    name: 'charpic',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'DMs you with another user\'s character picture.',
    aliases: ['charpick', 'characterpic', 'characterpick'],  //if no aliases remove this line
    args: true,     //does this function require any args?
    gmonly: false,  //make true if only GMs should be able to use this command
    usage: '<@username of other user>', //If there are args, please fill in this line with the proper order of arguments, if not, remove this line.

    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            let dt = require('../mymodules');
            const {CharPics} = require('../dbObjects');  //These two lines only required if this command
            const {Op} = require('sequelize');           //references database objects.

            let user = message.mentions.users.first();
            if (user === undefined) {
                return message.reply(`You forgot to mention a user.`)
            }
            let userID = user.id;
            let serverID = message.guild.id;

            const picEntry = await CharPics.findOne({                   //Check if user already submitted for this server
                where: {server: serverID, user: userID},
            });

            if (picEntry) {
                let msg = `On server ${message.guild}, ${user} is playing ${picEntry.charinfo}.\n${picEntry.picurl}`
                return message.author.send(msg)//return message.author.send(data, {split: true})
                    .then(() => {
                        if (message.channel.type === 'dm') return;
                        message.reply('I\'ve sent you a DM!');
                    })
                    .catch(error => {
                        console.error(`Could not send picture DM to ${message.author.tag}.\n`, error);
                        message.reply('it seems like I can\'t DM you! Do you have DMs disabled?');
                    });
                //message.reply(`Found it, ${msg}`)
            } else {
                message.reply(`It looks like they haven't uploaded a picture.`)
            }


        })();  //use to close the async if you're using it.
    },
};