module.exports = {
    name: 'd6',  //name of command in lowercase
    description: 'Rolls 1d6',
    args: false,
    gmonly: false,
    usage: '',

    execute(message, args) {
        var dt = require('../mymodules');

        return message.reply(`On 1d6 you rolled a ${dt.GetRandomNumber(6)}`);

    },
};