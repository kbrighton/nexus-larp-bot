module.exports = {											//This was a joke for ANext 2020, that didn't get used.
    name: 'playkcd',										//It would have turned all RPS throws into KCD throws
    description: 'Change the global PlayKCD variable',
    args: true,
    gmonly: true,
    usage: '<true/false>',
    execute(message, args) {
        var dt = require('../mymodules');


        if (args[0] == "true") {
            PlayKCD = true;
        } else {
            PlayKCD = false
        }

        message.reply(`Set to ${PlayKCD}`);


    },
};