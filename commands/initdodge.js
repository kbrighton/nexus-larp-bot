module.exports = {
    name: 'initdodge',  //name of command in lowercase
    description: `Stores a player's Initiative and Dodge in a database for easier listing
    If you screw up, you can use !initclear to remove your mistake from the list and !initdodge again`,
    args: true,
    gmonly: false,  //make true if only GMs should be able to use this command
    usage: '<Your Initiative (Brains + Athletics)> <Your Dodge> <(Optional) If you have a pet or other acting on its own intiative, run the command again with the pet\'s name here>',

    execute(message, args) {
        (async () => {
            var dt = require('../mymodules');
            const {InitDodge} = require('../dbObjects');
            const {Op} = require('sequelize');

            let re = /[0-9]{1,2}/g;
            const argsString = args.join(' ');
            var Nums = argsString.matchAll(re);
            Nums = Array.from(Nums);
            var initNum = parseInt(Nums[0]);
            var dodgeNum = parseInt(Nums[1]);
            var PetName = argsString.replace(re, "").replace(/^[ \t]+|[ \t]+$/, "");
            //console.log(`PetName = "${PetName}"`);
            var UseName = message.member.guild.members.cache.get(message.author.id).displayName.toString();
            if (PetName.length) {
                UseName = PetName + '(' + UseName + ')'
            }

            if (isNaN(initNum) || isNaN(dodgeNum)) {
                return message.react('❓');
                //return message.reply('I did not understand that.');
            }

            const InitEntry = await InitDodge.findOne({                             //Check if user already submitted for this channel
                where: {channel: message.channel.id.toString(), name: UseName},
            });

            if (InitEntry) {                                                        //if user already submitted, update
                InitEntry.dodge = dodgeNum;
                InitEntry.init = initNum;
                InitEntry.save()
                //.then(message.reply(`Updated ${UseName} to Init: ${initNum} Dodge: ${dodgeNum}`))

            } else {

                await InitDodge.create({
                    channel: message.channel.id.toString(),
                    name: UseName,
                    init: initNum,
                    dodge: dodgeNum,
                });
                //message.reply(`Added to InitDodge table: ${UseName} Init: ${initNum} Dodge: ${dodgeNum}`);
            }

            message.delete();

        })();
    },
};