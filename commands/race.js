module.exports = {
    name: 'race',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'Sets speed for one round of a race.  Uses the ',
    aliases: ['ALIAS1', 'ALIAS2'],  //if no aliases remove this line
    args: true,     //does this function require any args?
    gmonly: false,  //make true if only GMs should be able to use this command
    usage: '<arg1> <arg2>', //If there are args, please fill in this line with the proper order of arguments, if not, remove this line.

    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            let dt = require('../mymodules');
            const {InitDodge} = require('../dbObjects');  //These two lines only required if this command
            const {Op} = require('sequelize');           //references database objects.

            //Test Pilot Vehicle vs target speed
            //No test to maintain speed
            //If slowing down get a bonus to Pilot Vehicle
            //Fail = maintain speed
            //Critfail = loose 1 speed

            let PV = args.shift();

            switch (PV.toLowerCase()) {  //To make life easier for players, check if they put a space in the command name
                case "setup":
                    return message.client.commands.get('racesetup').execute(message, args);
                    break;
                case "update":
                    return message.client.commands.get('raceupdate').execute(message, args);
                    break;
                case "stage":
                    return message.client.commands.get('racestage').execute(message, args);
                    break;
                case "list":
                    return message.client.commands.get('racelist').execute(message, args);
                    break;
                case "m":				//Maintaining speed, so just add this number
                    PV = "-1";
                    break;
                case "s":				//Slowing speed, so just set to this number
                    PV = "-2";
                    break;
            }

            let Speed = parseInt(args.pop());
            let oldSpeed = 1;
            let Change = 0;
            let Win = "T"
            let UseName = message.guild.member(message.author).displayName;

            const RaceEntry = await InitDodge.findOne({                             //Check if user already submitted for this channel
                where: {channel: message.channel.id.toString(), name: UseName},
            });

            if (RaceEntry) {
                message.reply(`Already in, dodge is: ${RaceEntry.dodge}`)
                if (RaceEntry.init === -1) {
                    oldSpeed = 1
                } else {
                    oldSpeed = RaceEntry.init
                }
            }

            PV = parseInt(PV);
            message.reply(`PV = ${PV} and Speed = ${Speed}`)

            if (isNaN(PV) || isNaN(Speed)) {
                return message.reply(`I didn't understand that.`)
            }

            if (PV > -1) {
                Win = dt.RpsDoIWin(message, PV, Speed);
                if (Win === "C") {
                    if (oldSpeed === 0) {
                        Speed = 0
                    } else {
                        Speed = oldSpeed - 1
                    }
                } else if (Win === "F") {
                    Speed = oldSpeed;
                }
            } /*else if (PV === -1) {

			} else if (PV === -2) {

			}*/

            Change = Speed - oldSpeed;

            if (RaceEntry) {                                                        //if user already submitted, update
                RaceEntry.dodge = Change;
                RaceEntry.init = Speed;
                RaceEntry.save()
                    .then(message.reply(`Updated ${UseName} to Speed: ${Speed} Change: ${Change}`))

            } else {

                await InitDodge.create({
                    channel: message.channel.id.toString(),
                    name: UseName,
                    init: Speed,
                    dodge: Change,
                });
                message.reply(`Added to InitDodge table: ${UseName} Speed: ${Speed} Change: ${Change}`);
            }

            message.delete();


        })();  //use to close the async if you're using it.
    },
};