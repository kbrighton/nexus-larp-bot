module.exports = {
    name: 'help',
    description: 'List all of my commands or info about a specific command.',
    aliases: ['commands'],

    gmonly: false,
    execute(message, args) {
        var dt = require('../mymodules');
        const {prefix, botID} = require('../config.json');
        const data = [];
        const {commands} = message.client;
        const Discord = require('discord.js');
        const helpEmbed = new Discord.EmbedBuilder;
        let gmOnly = false;
        let userGM = message.member.roles.cache.some(role => role.name === `GMs`) || message.member.roles.cache.some(role => role.name === `AGMs`);

        if (!args.length) {
            /*			data.push('**Command List:**');
                        data.push('To recieve a DM with the LARP rules use \`!rules\`');
                        data.push('To throw Rock, Paper, or Scissors once use \`!rps\`');
                        data.push('To throw an Excelence Test at # use \`!rpsET #\`');
                        data.push('--With your skill level for the #; eg. !rpsET 5');
                        data.push('To throw against another person or a set difficulty use \`!rpsVS # <name> #\`');
                        data.push('--With your skill level, the name of the opponent (or just "Difficulty"), and their skill level; eg !rpsVS 1 @Beth 4');
                        data.push('If <@' + botID + '> asks you do to a bulk test use \`!bulk # #\`');
                        data.push('--With your skill level, and the difficulty level supplied by the bot; eg !bulk 4 3');
                        data.push('--The bot will react with an emoji if you ✅Passed; ❌Failed; or ‼️CritFailed, then click that emoji on the bot\'s message');
                        data.push('--If the bot marks your !bulk with ❓ it didn\'t understand you.');
                        data.push('For a random number use \`!rng #\`');
                        data.push('--With the maximum number for the #; eg. !rng 30');
                        data.push('To roll a single die, use \`!d6\`; \`!d10\`; \`!d20\`; or \`!d100\`; or roll 2d6 with \`!2d6\`');
                        data.push('For more info about any one command use \`!help <command>\`')
            */
            helpEmbed
                .setColor('#FF0000')
                .setTitle(`NexusLarpBot Command List:`)
                //.setURL('https://bitbucket.org/kbrighton/nexus-larp-bot/src/master/')
                .setDescription('Words inside <> denote a single variable, you do not need to type the <>\nRock/Paper/Scissors means please type one of Rock Paper or Scissors')
                .addFields(
                    {name: 'To throw Rock, Paper, or Scissors once use \`!rps\`', value: '\u200B'},
                    {
                        name: 'To throw an Excellence Test at # use \`!rpsET #\`',
                        value: '--With your skill level for the #; eg. !rpsET 5'
                    },
                    {
                        name: 'To throw against another person or a set difficulty use \`!rpsVS # <name> #\`',
                        value: '--With your skill level, the name of the opponent (or just "Difficulty"), and their skill level; eg !rpsVS 1 @Beth 4'
                    },
                    {
                        name: `If I ask you do to a bulk test use \`!bulk # #\``,
                        value: '--With your skill level, and the difficulty level supplied by the bot; eg !bulk 4 3\n--The bot will react with an emoji if you ✅Passed; ❌Failed; or ‼️CritFailed, then click that emoji on the bot\'s message\n--If the bot marks your !bulk with ❓ it didn\'t understand you.'
                    },
                    {
                        name: '\`NEW\`: To do a contested throw against another player use \`!contested <@Username> <Rock/Paper/Scissors> \`',
                        value: '--With the username of the person you are challengeing, and your throw.\n--I\'ll ask them to send Rock, Paper, or Scissors, and display the winner.\n--\`Please make sure to spell correctly or I won\'t understand you.\`'
                    },
                    {
                        name: '\`NEW\`: I can store your character\'s picture, and display it to other players!',
                        value: '--Store your picture by posting in the Introductions channel \`!mycharpic <Character Name>  <Character Series> + Attach a photo or url of a photo of your character \`\n--To see someone else\'s character use \`!charpic <@Username>\` and I\'lll DM you their picture'
                    },
                    {
                        name: 'For a random number use \`!rng #\`',
                        value: '--With the maximum number for the #; eg. !rng 30'
                    },
                    {
                        name: 'To roll a single die, use:',
                        value: ' \`!d6\`; \`!d10\`; \`!d20\`; or \`!d100\`; or roll 2d6 with \`!2d6\`'
                    },
                    {name: 'For more info about any one command use \`!help <command>\`', value: '\u200B'},
                )
                .setFooter({text: 'NexusLarpBot ©2019-2020 @RichterCa#3142, @kbrighton#1150', iconUrl: 'https://i.imgur.com/RhhkOpR.png'});

        } else {
            const name = args[0].toLowerCase();
            const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

            if (!command) {
                return message.reply('that\'s not a valid command!');
            }

            if (command.gmonly && !userGM) return message.reply('no cheating, that\'s for GMs only!');

            /*            data.push(`**Name:** ${command.name}`);
                        if (command.aliases) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
                        if (command.usage) data.push(`**Usage:** ${prefix}${command.name} ${command.usage}`);
                        if (command.description) data.push(`**Description:** ${command.description}`);*/

            helpEmbed
                .setColor('#FF0000')
                .setTitle(`**Command Name:** ${command.name}`);
            if (command.aliases) helpEmbed.addField(`**Aliases:**`, `${command.aliases.join(', ')}`, false);
            if (command.usage) helpEmbed.addField(`**Usage:**`, `${prefix}${command.name} ${command.usage}`, false);
            if (command.description) helpEmbed.setDescription(`${command.description}`);
            if (command.gmonly) gmOnly = true;

        }


        //data.push(dt.CommandList(message));

        if (message.channel.type === 'dm') {
            return message.author.send({embeds: [helpEmbed]});//return message.author.send(data, {split: true})
        } else {


            if (userGM) {

                if (gmOnly) {
                    const channel = message.guild.channels.cache.find(channel => channel.name === "gm-bot-help");

                    if (channel == null) {
                        return message.reply(`There is no channel for me to send the commands to.  Please make a channel called "#gm-bot-help"`)
                    }

                    channel.send({embeds: [helpEmbed]});
                } else {

                    message.channel.send({embeds: [helpEmbed]});//message.reply(data);
                }

            } else {

                return message.author.send({embeds: [helpEmbed]})//return message.author.send(data, {split: true})
                    .then(() => {
                        if (message.channel.type === 'dm') return;
                        message.reply('I\'ve sent you a DM with my commands!');
                    })
                    .catch(error => {
                        console.error(`Could not send commands DM to ${message.author.tag}.\n`, error);
                        message.reply('it seems like I can\'t DM you! Do you have DMs disabled?');
                    });
            }
        }
    },
};