module.exports = {
    name: 'bulksetup',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'Tells people how to do a bulk RPS test, then posts the three response emojis so people can click them for counting purposes.',
    aliases: ['rpsbulksetup', 'masssetup', 'rpsmasssetup'],  //if no aliases remove this line
    args: true,     //does this function require any args?
    gmonly: false,  //make true if only GMs should be able to use this command
    usage: '<Skill Name> <Diff #>', //If there are args, please fill in this line with the proper order of arguments, if not, remove this line.

    execute(message, args) {

        let dt = require('../mymodules');
        const Discord = require('discord.js');

        var Diff = parseInt(args.pop());
        var Skill = args.join(' ');

        if (isNaN(Diff)) {
            let reply = `I didn\'t understand that, ${message.author}!`
            reply += '\nThe proper usage would be: \`!bulksetup <Skill Name> # (Difficulty) \`';
            message.channel.send(reply);
        } else {
            const bulkEmbed = new Discord.MessageEmbed()
                .setColor('#ffff00')
                .setTitle(`BULK \`${Skill.toUpperCase()}\` THROW AT A \`${Diff}\`!`)
                .setDescription(`EVERYONE PLEASE TYPE "!bulk <Your **\`${Skill}\`** skill number> ${Diff}" i.e. \`!bulk x ${Diff}\``)
                .addField(`I will react with an emoji if you ✅Passed; ❌Failed; or ‼️CritFailed.`, 'Click that emoji on this message so the GM can keep track.', true);

            message.channel.send({embeds: [bulkEmbed]}).then(sentEMB => {
                sentEMB.react('✅');
                sentEMB.react('❌');
                sentEMB.react('‼️');
            });


            message.delete();
        }

    },
};