module.exports = {
    name: 'initclear',
    description: 'Clear your initiative (and any allies) in case of a problem, and you can re-submit using !initdodge',
    args: false,
    gmonly: false,


    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            var dt = require('../mymodules');
            const {InitDodge} = require('../dbObjects');  //These two lines only required if this command
            const {Op} = require('sequelize');           //references database objects.


            await InitDodge.destroy({                         //This block empties the db of any previous entries
                where: {                                      //by this user from only this channel
                    channel: message.channel.id.toString(),
                    name: {
                        [Op.substring]: message.member.guild.members.cache.get(message.author.id).displayName.toString()
                    },
                }
            });

        })();  //use to close the async if you're using it.
    },
};