module.exports = {
    name: 'rps',
    description: 'Throw one random RPS',
    args: false,
    gmonly: false,
    aliases: ['rsp'],

    execute(message, args) {
        //Since people keep typing "!rps et" and "!rps vs" with a space,
        //this function now checks for that, and redirects, if necessary.

        var dt = require('../mymodules');

        if (args.length) {

            const command = args.shift().toLowerCase();
            if (command == 'vs') {
                //message.reply('VS');
                message.client.commands.get('rpsvs').execute(message, args);
            } else if (command == 'et') {
                //message.reply('ET');
                message.client.commands.get('rpset').execute(message, args);
            } else if ((command == 'bulk') || (command == 'mass')) {
                message.client.commands.get('bulk').execute(message, args);
            } else {
                dt.JustOneThrow(message)
            }
        } else {
            dt.JustOneThrow(message)
        }
    },
};