module.exports = {
    name: '2d6',  //name of command in lowercase
    description: 'Rolls 2d6',
    args: false,
    gmonly: false,
    usage: '',

    execute(message, args) {
        var dt = require('../mymodules');

        var Total = dt.GetRandomNumber(6) + dt.GetRandomNumber(6);

        return message.reply(`On 2d6 you rolled a ${Total}`);

    },
};