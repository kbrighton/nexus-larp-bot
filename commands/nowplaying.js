module.exports = {
    name: 'nowplaying',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: `Change the bot's "Playing" status to whatever you like, such as the name of your larp, just make it short.`,
    args: true,     //does this function require any args?
    gmonly: true,  //make true if only GMs should be able to use this command
    usage: '<Name of your larp>', //If there are args, please fill in this line with the proper order of arguments, if not, remove this line.

    execute(message, args) {

        let dt = require('../mymodules');
        let status = args.join(' ');
        const {botID} = require('../config.json');
        message.client.users.cache.get(botID).setActivity(status, {type: 'PLAYING'});

    },
};