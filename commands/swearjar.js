module.exports = {
    name: 'swearjar',
    description: 'Prints the current Spazzy Swear Jar total',
    aliases: ['spazzyswearjar'],
    args: false,
    gmonly: true,
    usage: '[command name]',
    execute(message, args) {
        var dt = require('../mymodules');


        const SpazzyID = "311145912837603331";
        var fs = require("fs");
        var text = fs.readFileSync("./SwearJar.txt", "utf-8");
        var ANumber = parseFloat(text);

        message.channel.send(`The <@${SpazzyID}> Swear Jar is now at $${ANumber.toFixed(2)}`);


    },
};