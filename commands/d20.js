module.exports = {
    name: 'd20',  //name of command in lowercase
    description: 'Rolls 1d20',
    args: false,
    gmonly: false,

    execute(message, args) {
        var dt = require('../mymodules');

        return message.reply(`On 1d20 you rolled a ${dt.GetRandomNumber(20)}`);

    },
};