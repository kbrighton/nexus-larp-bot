module.exports = {
    name: 'addrole',
    description: 'Add a role to all server members',
    args: true,
    gmonly: true,
    usage: '<role name> (Case Sensative)',
    execute(message, args) {
        var dt = require('../mymodules');

        const role = message.guild.roles.cache.find(role => role.name === args[0]);

        if (role == undefined) {
            return message.reply("I can't find that role.");
        } else {
            message.guild.members.cache
                .each(GuildMember => {
                    //console.log(GuildMember.user.username)
                    GuildMember.roles.add(role);
                });
        }

        message.reply(`I added the role ${args[0]} to all members of this server.`);


    },
};