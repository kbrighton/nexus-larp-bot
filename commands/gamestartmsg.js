module.exports = {
    name: 'gamestartmsg',
    description: 'Sends a message to every member of the guild (server).  Useful for just before Game On on Friday./nNOTE: I should probably add a way to easily view and change this for future larps...',
    args: false,
    gmonly: true,

    execute(message, args) {
        var dt = require('../mymodules');

        const msg = dt.WelcomeMessage();


        msg.push('It is now just about time for Game On for the NekoLARP 2021 Online LARP!');
        msg.push('If you haven\'t already, be sure to check your character folder for your teaser.');
        //msg.push('To get you started, please watch this informational video:  https://youtu.be/vxlG5JmJETw');
        msg.push('Also note: Organizing yourselves into groups will be very important for this LARP, so we\'ve given you the ability to join roles to help show where want to go.');
        msg.push('We\'re going to be sending out some more documents and forms soon with some rules and questions about actions and groups in this LARP, so be on the look out.');


        message.guild.members.fetch().then(fetchedMembers => {
            const totalRole = fetchedMembers;
            // We now have a collection with all online member objects in the totalRole variable
            //message.channel.send(`There are currently ${totalRole.size} members online in this guild!`);

            for (var value of totalRole.values()) {
                value.user.send(msg, {split: true});
            }
        });


    },
};