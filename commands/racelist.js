module.exports = {
    name: 'racelist',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'DESCRIPTION',
    aliases: ['ALIAS1', 'ALIAS2'],  //if no aliases remove this line
    args: false,     //does this function require any args?
    gmonly: false,  //make true if only GMs should be able to use this command
    usage: '<arg1> <arg2>', //If there are args, please fill in this line with the proper order of arguments, if not, remove this line.

    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            let dt = require('../mymodules');
            const {InitDodge} = require('../dbObjects');  //These two lines only required if this command
            const {Op} = require('sequelize');           //references database objects.
            const Discord = require('discord.js');


            const raceList = await InitDodge.findAll({        					//Querries the DB, and returns a collection of
                where: {																//INIT results from this channel
                    [Op.and]: [
                        {channel: [message.channel.id.toString()]},
                        {init: {[Op.gt]: -1,}}
                    ]

                },
                order: [
                    ['init', 'DESC'],
                    ['name', 'ASC'],
                ],
                attributes: ['key_id', 'init', 'dodge', 'name',]
            },);
            if (!raceList.length) return message.channel.send(`Race List Empty`);

            /*			MSG = "Race LIST:\n";
                        for (var i = 0, len = raceList.length; i < len; i++) {
                            MSG += `${raceList[i].name}: ${raceList[i].init}; change of ${raceList[i].dodge} \n`;
                        }

                        message.channel.send(MSG, {code: true});*/


            let raceListEmbed = new Discord.MessageEmbed;
            raceListEmbed
                .setColor('#ff00FF')
                .setTitle(`Current Race Standings`)

            var iCount = 0;
            var prevSpeed = 0;															//Loops through the collection, to put multiple users with
            var MSG = "";												//the same init on the same line (to save space).
            for (var i = 0, len = raceList.length; i < len; i++) {
                //MSG += `${raceList[i].name}: ${raceList[i].init}, change of ${raceList[i].dodge} `;
                console.log(MSG);
                if (raceList[i].init === prevSpeed) {
                    MSG += `${raceList[i].name}: ${raceList[i].init}, change of ${raceList[i].dodge} `
                } else {
                    if (iCount > 0) {
                        raceListEmbed.addField(`Speed ${prevSpeed}`, `${MSG}`)
                    }
                    MSG = `${raceList[i].name}: ${raceList[i].init}, change of ${raceList[i].dodge} \n`;
                    //MSG = "test"
                    iCount += 1;
                }
                prevSpeed = raceList[i].init;
            }
            raceListEmbed.addField(`Speed ${prevSpeed}`, `${MSG}`);
            message.channel.send(raceListEmbed);

        })();  //use to close the async if you're using it.
    },
};