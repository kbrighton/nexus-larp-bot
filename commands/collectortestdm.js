module.exports = {
    name: 'collectortestdm',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'Testing DMs with collector tests',
    args: true,     //does this function require any args?
    gmonly: false,  //make true if only GMs should be able to use this command

    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            let dt = require('../mymodules');
//          const { InitDodge } = require('../dbObjects');  //These two lines only required if this command
//          const { Op } = require('sequelize');           //references database objects.

            let User1 = message.author;
            let User2 = message.mentions.users.first();
            const data = [];

            //TO DO: Not in this command, but you'll need to create a new Database RPSVS with 5 fields "KeyID", "User1", "Response1", "User2", "Response2"
            //TO DO:  Add a new entry to RPSVS database get and store KeyID in variable

            message.channel.send(`${User1} has challenged ${User2} to a contested throw!  Please check your DMs and respond with your throw!`)

            User1.createDM().then(c => {
                c.send(`You are in a contested throw with ${User2}\nReply to this message with "Rock", "Paper" or "Scissors", please.`, {split: true});
                const filter = m => m.content.includes('R') || m.content.includes('P') || m.content.includes('S');
                const collector1 = c.createMessageCollector(filter, {time: 30000});
                //TO DO:  Error handling if they enter something without R, P, or S or time runs out
                collector1.on('collect', m => {
                    console.log(`Collected ${m.content} from User1`)
                });
                collector1.on("end", collected => {
                    //TO DO: Insert into RSPVS database User1 and Response1 at KeyID
                    //TO DO: See if both entries in RSPVS database at KeyID are entered, if so run function to check them and return winner
                });
            });

            User2.createDM().then(d => {
                d.send(`You are in a contested throw with ${User2}\nReply to this message with "Rock", "Paper" or "Scissors", please.`, {split: true});
                const filter = n => n.content.includes('R') || n.content.includes('P') || n.content.includes('S');
                const collector2 = d.createMessageCollector(filter, {time: 30000});
                //TO DO:  Error handling if they enter something without R, P, or S or time runs out
                collector2.on('collect', n => {
                    console.log(`Collected ${m.content} from User2`)
                });
                collector2.on("end", collected => {
                    //TO DO: Insert into RSPVS database User2 and Response2 at KeyID
                    //TO DO: See if both entries in RSPVS database at KeyID are entered, if so run function to check them and return winner
                });
            });


        })();  //use to close the async if you're using it.
    },
};