module.exports = {
    name: 'initsetup',
    description: 'Sets up a new Initiative and Dodge list for the channel, asks users for input.',
    aliases: ['listsetup', 'combatsetup'],
    args: false,
    gmonly: true,

    execute(message, args) {

        (async () => {
            const {InitDodge} = require('../dbObjects');
            const {Op} = require('sequelize');
            const Discord = require('discord.js');
            var dt = require('../mymodules');
            const {botID} = require('../config.json');
            const util = require('util')

            message.channel.messages.fetchPinned()            //This block removes all messages previously pinned to
                .then(messages => {                           //the channel by this bot.
                    //console.log(`Collected ${messages.size} items ` + require('util').inspect(messages) );
                    messages
                        .filter(msg => ((msg.author.id == botID) && (msg.content.includes('LIST'))))
                        .each(msg => msg.unpin())
                })
                //.then(message.reply('Removed all my pins'))
                .catch(console.error);
            const data = [];

            await InitDodge.destroy({                         //This block empties the db of any previous entries
                where: {                                      //from only this channel
                    channel: message.channel.id.toString(),
                }
            });
            //message.reply('Init list cleared for this channel.');
            /*
                        data.push(`EVERYONE PLEASE LIST YOUR INITIATIVE AND DODGE USING THE FOLLOWING COMMAND:`);
                        data.push(`\`!InitDodge # # \` with your initiative and dodge numbers`);
                        data.push(`If you have a companion or ally that acts on a different init from you, do the command again with the companion's name at the end.`);

                        message.channel.send(data);
            */

            const initEmbed = new Discord.MessageEmbed()
                .setColor('#ffFF00')
                .setTitle(`EVERYONE PLEASE LIST YOUR INITIATIVE AND DODGE USING THE FOLLOWING COMMAND:`)
                //.setDescription(`EVERYONE PLEASE TYPE "!bulk <Your **\`${Skill}\`** skill number> ${Diff}" i.e. \`!bulk 3 ${Diff}\``)
                .addField(`\`!InitDodge # # \` with your initiative and dodge numbers`, `If you have a companion or ally that acts on a different init from you, do the command again with the companion's name at the end.`, true);

            message.channel.send({embeds: [initEmbed]});

            message.delete();

        })();

    },
};