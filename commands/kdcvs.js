module.exports = {
    name: 'kcdvs',
    description: 'Play one round of KCD against another player',
    args: true,
    gmonly: false,
    usage: '@<person>',
    execute(message, args) {

        if (!(message.channel.id == '700512660461977682')) return message.channel.send(`I'm sorry ${message.author}, I can't let you do that.`);
        //This only works in the KCD channel on the ANext Server.

        var dt = require('../mymodules');
        var AName = message.author;
        var BName = args[0];
        var MatchString = `KCD Match: ${message.author} vs ${BName}
Remember, K-9 bites Cow, Deputy tases K-9, Cow kicks Deputy.`;

        var matchNo = 0;
        message.channel.send(MatchString + `must win one throw outright to win the match`);
        do {
            matchNo = matchNo + 1;
            Mwin = dt.OneMatch(message, AName, BName, matchNo, true, true);
        } while (Mwin == 'T');
        if (Mwin == 'A') {
            AllWin = 'P'
        } else {
            AllWin = 'F'
            if ((Mwin == 'B') & (matchNo == 1)) {
                AllWin = 'C'
            }
        }

        if (AllWin == 'P') {
            message.reply(`you won your KCD Match vs ${BName}!`);
        } else {
            var MSG = ` sorry, you lost your KCD Match vs ${BName}.`;
            message.reply(MSG);
        }

    },
};