module.exports = {
    name: 'bulk',
    description: `For when large groups of players need to make throws, use !bulk # #
    At the GM's request the bot will ask you to make a bulk test of a specific Skill, and give you the difficulty you must beat. 
The bot will react with an emoji if you ✅Passed; ❌Failed; or ‼️CritFailed; if it responds with ❓ it didn't understand you.`,
    aliases: ['rpsbulk', 'mass', 'rpsmass'],
    args: true,
    gmonly: false,
    usage: '<Your Skill #> <The Difficulty>',
    execute(message, args) {
        var dt = require('../mymodules');

        var AName = message.author;

        let re = /[0-9]{1,2}/g;										//Use RegEx to pull out the first two numbers then
        const argsString = args.join(' ');							//remove those numbers, and set BName to any non-number words in args
        var Nums = argsString.matchAll(re);
        Nums = Array.from(Nums);
        var ANumber = parseInt(Nums[0]);
        var BNumber = parseInt(Nums[1]);
        var BName = argsString.replace(re, "").replace(/^[ \t]+|[ \t]+$/, "");  //pulls out leading or ending spaces

        if (isNaN(ANumber) || isNaN(BNumber)) {
            let reply = `I didn\'t understand that, ${message.author}!`
            message.react('❓');
            return
        }

        var matchNo = 0;
        var Mwin = 'T';
        var AllWin = 'F';
        var CritF = false;

//		var MatchString = `RSP Match: ${message.author} at a ${ANumber} vs ${BName} at a ${BNumber} = ${message.author} `;
//		var MatchType = `RPS`;

        if (ANumber === BNumber) {
//			message.channel.send(MatchString + `must win one throw outright to win the match`)
            do {
                matchNo = matchNo + 1;
                Mwin = dt.OneMatch(message, AName, BName, matchNo, false, false);
            } while (Mwin == 'T');
            if (Mwin == 'A') {
                AllWin = 'P'
            } else {
                AllWin = 'F'
                if ((Mwin == 'B') & (matchNo == 1)) {
                    AllWin = 'C'
                }
            }
        } else if (ANumber > BNumber) {
//			message.channel.send(MatchString + `must win or tie at least one throw out of ${(ANumber - BNumber)} to win the match`)
            AllWin = dt.RpsRoundHigher(message, AName, ANumber, BName, BNumber, false);
        } else {
//			message.channel.send(MatchString + `must win all of ${(BNumber - ANumber)} throws to win the match`)
            AllWin = dt.RpsRoundLower(message, AName, ANumber, BName, BNumber, false);
        }

        if (AllWin == 'P') {
//			message.reply(`you \`won\` your ${MatchType} Match vs ${BName}!`);
            message.react('✅');
//			message.author.send('You PASSED your bulk test ' + BName + ' at difficulty ' + BNumber + '.  Please click the ✅ emoji.', { split: true });
        } else {
//			var MSG = ` sorry, you \`lost\` your ${MatchType} Match vs ${BName}.`;
            if (AllWin == 'C') {
//				dt.CritFailState(message, MSG) 
                if (dt.TestCritFail(message, false)) {
                    message.react('‼️');
//					 message.author.send('You CRITICALLY FAILED your bulk test ' + BName + ' at difficulty ' + BNumber + '.  Please click the ‼️ emoji.', { split: true });
                } else {
                    message.react('❌');
//					 message.author.send('You FAILED your bulk test ' + BName + ' at difficulty ' + BNumber + '.  Please click the ❌ emoji.', { split: true });
                }
            } else {
//				message.reply (MSG)
                message.react('❌');
//				 message.author.send('You FAILED your bulk test ' + BName + ' at difficulty ' + BNumber + '.  Please click the ❌ emoji.', { split: true });
            }
        }

        //message.delete();
    },
};