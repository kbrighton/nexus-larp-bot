module.exports = {
    name: 'contested',
    description: 'Make a contested throw against another player, both choosing Rock, Paper, or Scissors.',
    aliases: ['contestedthrow', 'contestedrps', 'contestedrsp'],
    args: true,
    gmonly: false,
    usage: '<@Username of other person> <Rock/Paper/Scissors>',

    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            let dt = require('../mymodules');
            const {prefix, botID} = require('../config.json');

            const answers = ["rock", "paper", "scissors", "r", "p", "s"]
            let User1 = message.author;
            let User2 = message.mentions.users.first();
            let Throw1 = args.pop(); //RPS of whatever is in the message
            let Num1 = 1; //Num version of RPS
            let Throw2 = "Rock"; // will change to whatever is collected.
            let Num2 = 1; //Num version of RPS

            message.delete();  //delete the message so the other person can't see what was thrown

            //Error checking
            if (User2 === undefined) {
                return message.reply(`No other user challenged`);
            }
            if (!(answers.some(answer => answer.toLowerCase() === Throw1.toLowerCase()))) {
                return message.reply(`You did not pick Rock, Paper, or Scissors!`)
            }

            Num1 = answers.indexOf(Throw1.toLowerCase()) + 1;  	//Get the number for checking RPS, add one because I didn't use 0.
            if (Num1 > 3) {
                Num1 -= 3
            }							//Lets people enter just 'r' 'p' or 's'

            const filter = (response) => {    //Filters responses so only a response from the challenged person with only "rock" "paper" or "scissors" in it gets through.
                return response.author.id === User2.id && answers.some(answer => answer.toLowerCase() === response.content.toLowerCase());
            };

            message.channel.send(`${User1} challenges ${User2} to an RPS match!\n${User2}, please type "Rock", "Paper", or "Scissors"`).then(() => {
                message.channel.awaitMessages(filter, {max: 1, time: 45000, errors: ['time']})
                    .then(collected => {
                        collected.first().delete();
                        Throw2 = collected.first().content;
                        Num2 = answers.indexOf(Throw2.toLowerCase()) + 1;  //Get the number for checking RPS, add one because I didn't use 0.
                        if (Num2 > 3) {
                            Num2 -= 3
                        }							//Lets people enter just 'r' 'p' or 's'

                        let rGroup = dt.GetRandomNumber(5)
                        let Win = dt.Winner(Num1, Num2)
                        let WinName
                        if (Win == 'A') {
                            WinName = `${User1} wins this throw!`
                        } else if (Win == 'B') {
                            WinName = `${User2} wins this throw!`
                        } else {
                            WinName = 'It is a tie!'
                        }
                        var ResultString = `${User1} throws ${dt.ShowThrowResult(Num1, rGroup)} vs ${User2} throws ${dt.ShowThrowResult(Num2, rGroup)}! ${WinName}`;

                        return message.channel.send(ResultString);

                        //return message.channel.send(`${collected.first().author} got the correct answer: ${collected.first().content}!`);

                    })
                    .catch(collected => {
                        message.channel.send(`${User2} did not respond in time!  ${User1}, try again if you still want to challenge them.`);
                    });
            });


        })();  //use to close the async if you're using it.
    },
};