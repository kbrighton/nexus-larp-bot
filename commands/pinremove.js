module.exports = {
    name: 'pinremove',  //name of command in lowercase
    description: 'removes any pins made by the bot in this channel',

    args: false,
    gmonly: true,  //make true if only GMs should be able to use this command


    execute(message, args) {
        var dt = require('../mymodules');
        const {botID} = require('../config.json');

        message.channel.messages.fetchPinned()
            .then(messages => {
                messages
                    .filter(msg => (msg.author.id == botID))
                    .each(msg => msg.unpin())
            })
            .then(message.reply('Removed all my pins'))
            .catch(console.error);

    },
};