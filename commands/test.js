module.exports = {
    name: 'test',  //name of command in lowercase
    description: 'DESCRIPTION',
    aliases: ['ALIAS'],
    args: false,
    gmonly: false,
    usage: '[command name] + <args>', //If there are args, please fill in this line

    execute(message, args) {
        (async () => {
            var dt = require('../mymodules');
            let ls = require('../loggingsetup');

            let tempArray = ls.isLogGuild(message.guild.id.toString());
            if (tempArray !== -1) {
                message.reply(tempArray)
            } else {
                message.reply('Not logging this servers.')
            }
            const logChannel = message.client.channels.cache.get(tempArray[4]);
            message.reply(logChannel.toString());


        })();  //use to close the async if you're using it.
    },
};