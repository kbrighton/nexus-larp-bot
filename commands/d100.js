module.exports = {
    name: 'd100',  //name of command in lowercase
    description: 'Rolls 1d100',
    args: false,
    gmonly: false,

    execute(message, args) {
        var dt = require('../mymodules');

        return message.reply(`On 1d100 you rolled a ${dt.GetRandomNumber(100)}`);

    },
};