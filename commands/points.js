module.exports = {														//This function was for ANext 2020, and didn't get used
    name: 'points',
    description: 'Add, subtract, and display points for a user',

    gmonly: false,
    usage: '@<UserName> <add/sub/view> #',

    execute(message, args) {

        Array.prototype.searchFor = function (candid) {							//This quick function just runs a search in an array
            for (var i = 0; i < this.length; i++)									//returns the index if found, -1 if not
                if (this[i].indexOf(candid) == 0)
                    return i;
            return -1;
        };

        var dt = require('../mymodules');
        //This bit reads the current points array from a file.
        var fs = require("fs");
        var text = fs.readFileSync("./points.txt", "utf-8");
        var PointsArrayUF = text.split("\r\n");
        var PointsArray = PointsArrayUF.filter(Boolean);
        var UserID = message.author;
        var ANumber = parseInt(args[2]);
        var msgTail = '';

        //Only GMs can add or remove points, nonGMs only get their own total
        if (message.member.roles.cache.some(role => role.name === `GMs`) || message.member.roles.cache.some(role => role.name === `AGMs`)) {
            UserID = message.mentions.users.first();
        } else {
            if ((args[1] == 'sub') || (args[1] == 'add')) {
                msgTail = `= Sorry, only GMs can ${args[1]} points.`;
                args[1] = 'view';
            } else if (args[0] == null) {
                msgTail = ``;
            } else {
                msgTail = ` = Sorry, only GMs can view other peoples' points.`;
            }
        }


        if ((!(args[0] == null)) && ((!(args[1] == 'view') && isNaN(ANumber)) || UserID == null)) {
            let reply = `I didn\'t understand that, ${message.author}!`
            reply += '\nThe proper usage would be: \`!points @<userName> <add/sub/view> <#>\`';
            message.channel.send(reply);
            return
        }


        //console.log(PointsArray);

        var UserIndex = PointsArray.searchFor(UserID);					//Find if named person has points, then do whatever args to them.
        if (!(UserIndex == -1)) {
            //found, so add, subrtract or view points
            var ThisUser = PointsArray[PointsArray.searchFor(UserID)].split(/ +/);
            console.log(`Username: ${UserID} Data: ${ThisUser} Args: ${args[1]}`);


            switch (args[1]) {
                case 'view':
                default:
                    message.reply(`${UserID} has ${ThisUser[1]} points. ${msgTail}`);
                    break;
                case 'add':
                    var BNumber = parseInt(ThisUser[1]) + ANumber;
                    PointsArray[UserIndex] = `${UserID} ${BNumber}`;
                    message.reply(`${UserID} gained ${ANumber} points, and now has ${BNumber} points.`);
                    console.log(`After Add ${PointsArray}`);
                    break;
                case 'sub':
                    var BNumber = parseInt(ThisUser[1]) - ANumber;
                    PointsArray[UserIndex] = `${UserID} ${BNumber}`;
                    message.reply(`${UserID} lost ${ANumber} points, and now has ${BNumber} points.`);
                    console.log(`After Sub ${PointsArray}`);
                    break;
            }

        } else {
            //not found, add to array, or say "not found" if view
            console.log('User not found');
            switch (args[1]) {
                case 'view':
                default:
                    message.reply(`${UserID} has 0 points. ${msgTail}`);
                    break;
                case 'add':
                    PointsArray.push(`${UserID} ${ANumber}`);
                    message.reply(`${UserID} gained ${ANumber} points, and now has ${ANumber} points.`);
                    console.log(`After New Add ${PointsArray}`);
                    break;
                case 'sub':
                    var BNumber = 0 - ANumber;
                    PointsArray.push(`${UserID} ${BNumber}`);
                    message.reply(`${UserID} lost ${ANumber} points, and now has ${BNumber} points.`);
                    console.log(`After New Sub ${PointsArray}`);
                    break;
            }
        }

        //Everything below this line writes the data back to the file.
        const writeStream = fs.createWriteStream('points.txt');

        const pathName = writeStream.path;

// write each value of the array on the file breaking line
        PointsArray.forEach(value => writeStream.write(`${value}\r\n`));

// the finish event is emitted when all data has been flushed from the stream
        writeStream.on('finish', () => {
            console.log(`wrote all the array data to file ${pathName}`);
        });

// handle the errors on the write process
        writeStream.on('error', (err) => {
            console.error(`There is an error writing the file ${pathName} => ${err}`)
        });

// close the stream
        writeStream.end();


    },
};