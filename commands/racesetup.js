module.exports = {
    name: 'racesetup',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'Use to set up each round of a race.  Bot will ask the player to input their change of speed, and current Pilot Vehicle',
    //aliases: ['ALIAS1', 'ALIAS2'],  //if no aliases remove this line
    args: false,     //does this function require any args?
    gmonly: true,  //make true if only GMs should be able to use this command
    //usage: '<arg1> <arg2>', //If there are args, please fill in this line with the proper order of arguments, if not, remove this line.

    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            let dt = require('../mymodules');
            const {InitDodge} = require('../dbObjects');  //These two lines only required if this command
            const {Op} = require('sequelize');           //references database objects.

            const Discord = require('discord.js');
            const raceEmbed = new Discord.MessageEmbed;

            raceEmbed
                .setColor('#00ff00')
                .setTitle(`Gentlepersons, start your engines!`)
                .setDescription(`A new race is about to begin, so man your vehicles!`)
                .addFields(
                    {
                        name: `${message.author.username}, don't forget:`,
                        value: `Use \`!racestage\` to start each new stage of the race.\nUse \`!racelist\` to display the list in this channel.\nUse \`!racedisplay\` to display the list in the SOMETHING channel.`
                    },
                    {
                        name: `Racers:`,
                        value: `You'll start the race at Speed 1\nYou can change your speed by 2 each stage.`
                    },
                    {
                        name: `At the end of each stage:`,
                        value: `Please type \`!raceupdate <Current Speed>\`, so I can keep track of everything.`
                    },
                )


            message.channel.send(raceEmbed);
            message.delete();


            await InitDodge.destroy({                         //This block empties the db of any previous entries
                where: {                                      //from only this channel
                    channel: message.channel.id.toString(),
                }
            });


            /*			await InitDodge.update({ dodge: 0, init: -1 }, {  //For Race Purposes,Dodge is just change in speed round by round.
                            where: {
                                channel: message.channel.id.toString(),
                            }
                        });*/


        })();  //use to close the async if you're using it.
    },
};