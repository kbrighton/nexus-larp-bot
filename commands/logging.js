module.exports = {
    name: 'logging',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'Sets up logging preferences for a server.  These include logging all posts in the server to a specific channel, and logging to a file.',
    args: false,     //does this function require any args?
    gmonly: true,  //make true if only GMs should be able to use this command

    execute(message, args) {
        (async () => {
            var dt = require('../mymodules');
            let ls = require('../loggingsetup')
            const Discord = require('discord.js');
            let bool = true;

            let emojiPicked = '✅';
            const logEmbed = new Discord.MessageEmbed()
                .setColor('#ffFF00')
                .setTitle(`Logging on server: ${message.guild}`)

            //console.log(`in logging`)

            let tempArray = ls.isLogGuild(message.guild.id);

            if (tempArray !== -1) {

                message.channel.send(logEmbed).then(emb => {
                    ls.existsEmbed(message, emb)
                })
            } else {

                message.channel.send(logEmbed).then(emb => {
                    ls.firstEmbed(message, emb)
                })

            }

        })();  //use to close the async if you're using it.
    },
};