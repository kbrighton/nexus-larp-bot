module.exports = {
    name: 'load',  //name of command in lowercase
    description: 'Tell the bot to load a new command without rebooting the entire bot.',
    aliases: ['loadcommand', 'newcommand'],
    args: true,
    gmonly: true,
    usage: '<Name of Command to load> (Should be the same as the name of the .js file in ./commands/', //If there are args, please fill in this line

    execute(message, args) {
        var dt = require('../mymodules');

        //message.reply('In load');

        const commandName = args[0].toLowerCase();

        try {
            const newCommand = require(`./${commandName}.js`);
            message.client.commands.set(newCommand.name, newCommand);
            message.channel.send(`Command \`${commandName}\` was loaded!`);
        } catch (error) {
            console.log(error);
            message.channel.send(`There was an error while reloading a command \`${commandName}\`:\n\`${error.message}\``);
        }


    },
};