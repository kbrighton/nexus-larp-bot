module.exports = {
    name: 'gmhelp',
    description: 'List of GM specific commands to #gm-bot-help',
    aliases: ['gmcommands'],
    gmonly: true,
    execute(message, args) {
        var dt = require('../mymodules');
        const data = [];
        var stringData ="";
        const {commands} = message.client;

        data.push('GM ONLY Command List:');
        data.push('To set up a bulk (mass) RPS test use \`!bulksetup <Skill Name> <Difficulty Level>\` i.e. !bulksetup Jump 4');
        data.push('To message all users with a particular role use \`!dmrole <RoleName> <Message>\`');
        data.push('--<RoleName> is case sensative');
        data.push('To set up Channel Init and Dodge List use \`!initsetup\`');
        data.push('--Then after all players have input init/dodge, post a list with \`!initlist\`');
        data.push('Good old rock, nothing beats rock \`!rpsR\`');
        data.push('--*except \`!rpsP\`*');
        data.push('To give every member of the server a role use \`!addrole <RoleName>\`');
        data.push('To add, subtract, or view points use \`!points @<UserName> <add/sub/view> <#>\`');
        data.push('On Friday, to send a message to all members of the server use \`!gamestartmsg\`');
        data.push('To mass delete messages from a channel use \`!prune <number>\`');
        data.push('To change my Playing Status use \`!nowplaying <your larp name>\`')
        data.push('For more info on any command try \`!help <command>\`')

        data.forEach( datum=> {
            stringData += `${datum}\n`
        });

        if (message.channel.type === 'dm') {
            return message.author.send(stringData)
        } else {


            //const channel = message.guild.channels.cache.get('698176250547798047');
            const channel = message.guild.channels.cache.find(channel => channel.name === "gm-bot-help");

            if (channel == null) {
                return message.reply(`There is no channel for me to send the commands to.  Please make a channel called "#gm-bot-help"`)
            }

            channel.send(stringData);


//			for (var value of message.guild.channels.cache.values()) {
//				console.log(value);
//			};


        }
    },
};