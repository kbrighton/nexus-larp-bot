module.exports = {
    name: 'racem',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'For people who forget the space in "race m <Current Speed>"',
    //aliases: ['ALIAS1', 'ALIAS2'],  //if no aliases remove this line
    args: true,     //does this function require any args?
    gmonly: false,  //make true if only GMs should be able to use this command
    usage: '<Current Speed>', //If there are args, please fill in this line with the proper order of arguments, if not, remove this line.

    execute(message, args) {
//      (async () => {   //only use if code requires asnyc like .then or .await
        let dt = require('../mymodules');
//          const { InitDodge } = require('../dbObjects');  //These two lines only required if this command
//          const { Op } = require('sequelize');           //references database objects.

        args.unshift("m")

        return message.client.commands.get('race').execute(message, args);

        //code goes here


//		})();  //use to close the async if you're using it.
    },
};