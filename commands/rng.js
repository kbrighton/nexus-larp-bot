module.exports = {
    name: 'rng',  //name of command in lowercase
    description: 'Generate a random number between 1 and the number entered.',
    aliases: ['random'],
    args: true,
    gmonly: false,
    usage: '<max number>', //If there are args, please fill in this line

    execute(message, args) {
        var dt = require('../mymodules');

        var ANumber = parseInt(args[0]);
        if (isNaN(ANumber)) {
            return message.reply(`I didn't understand that, please use a number with this command.`);
        }

        return message.reply(`I randomly came up with: ${dt.GetRandomNumber(ANumber)}`);

    },
};