module.exports = {
    name: 'helpold',
    description: 'List all of my commands or info about a specific command.',
    aliases: ['commandsold'],

    gmonly: false,
    execute(message, args) {
        var dt = require('../mymodules');
        const {prefix, botID} = require('../config.json');
        const data = [];
        const {commands} = message.client;

        if (!args.length) {
            data.push('**Command List:**');
            data.push('To recieve a DM with the LARP rules use \`!rules\`');
            data.push('To throw Rock, Paper, or Scissors once use \`!rps\`');
            data.push('To throw an Excelence Test at # use \`!rpsET #\`');
            data.push('--With your skill level for the #; eg. !rpsET 5');
            data.push('To throw against another person or a set difficulty use \`!rpsVS # <name> #\`');
            data.push('--With your skill level, the name of the opponent (or just "Difficulty"), and their skill level; eg !rpsVS 1 @Beth 4');
            data.push('If <@' + botID + '> asks you do to a bulk test use \`!bulk # #\`');
            data.push('--With your skill level, and the difficulty level supplied by the bot; eg !bulk 4 3');
            data.push('--The bot will react with an emoji if you ✅Passed; ❌Failed; or ‼️CritFailed, then click that emoji on the bot\'s message');
            data.push('--If the bot marks your !bulk with ❓ it didn\'t understand you.');
            data.push('For a random number use \`!rng #\`');
            data.push('--With the maximum number for the #; eg. !rng 30');
            data.push('To roll a single die, use \`!d6\`; \`!d10\`; \`!d20\`; or \`!d100\`; or roll 2d6 with \`!2d6\`');
            data.push('For more info about any one command use \`!help <command>\`')


        } else {
            const name = args[0].toLowerCase();
            const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

            if (!command) {
                return message.reply('that\'s not a valid command!');
            }

            data.push(`**Name:** ${command.name}`);

            if (command.aliases) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
            if (command.usage) data.push(`**Usage:** ${prefix}${command.name} ${command.usage}`);
            if (command.description) data.push(`**Description:** ${command.description}`);

        }


        //data.push(dt.CommandList(message));

        if (message.channel.type === 'dm') {
            return message.author.send(data, {split: true})
        } else {

            if (message.member.roles.cache.some(role => role.name === `GMs`) || message.member.roles.cache.some(role => role.name === `AGMs`)) {

                message.reply(data);

            } else {

                return message.author.send(data, {split: true})
                    .then(() => {
                        if (message.channel.type === 'dm') return;
                        message.reply('I\'ve sent you a DM with my commands!');
                    })
                    .catch(error => {
                        console.error(`Could not send commands DM to ${message.author.tag}.\n`, error);
                        message.reply('it seems like I can\'t DM you! Do you have DMs disabled?');
                    });
            }
        }
    },
};