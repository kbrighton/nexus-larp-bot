module.exports = {
    name: 'mycharpic',  //name of command in lowercase  NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'Saves your Character Name, Series, and a picture to be accessible by other players',
    aliases: ['mycharacterpic', 'mycharacterpick', 'mycharpick'],  //if no aliases remove this line
    args: true,     //does this function require any args?
    gmonly: false,  //make true if only GMs should be able to use this command
    usage: '<Character Name> <Character Series> + Attach a photo or url of a photo of your character',

    execute(message, args) {
        (async () => {   //only use if code requires asnyc like .then or .await
            let dt = require('../mymodules');
            const {CharPics} = require('../dbObjects');  //These two lines only required if this command
            const {Op} = require('sequelize');           //references database objects.

            const {MessageAttachment} = require('discord.js');

            let URL = "none";
            let userID = message.author.id;
            let serverID = message.guild.id;

            if (message.attachments.size > 0) {
                const attachment = new MessageAttachment(message.attachments);
                URL = message.attachments.first().url
                //console.log(`Args: ${args}; LM: ${LM}`)
                //message.reply(`Args: ${args}`, attachment)
            }

            //URL = args.find((i) => {i.search("www")})
            let URLpos = args.findIndex(element => element.includes("htt"))
            if (URLpos !== -1) {
                URL = args.splice(URLpos, 1);
            }
            if (URL === "none") {
                return message.reply(`I don't think you attached an image.`)
            }
            let URL2 = URL.toString();
            let characterInfo = args.join(" ");
            //message.reply(`Args: ${args}  --  Found URL: ${URL}`)


            const picEntry = await CharPics.findOne({                   //Check if user already submitted for this server
                where: {server: serverID, user: userID},
            });

            if (picEntry) {												//Already in DB, so update.
                picEntry.picurl = URL2;
                picEntry.charinfo = characterInfo;
                picEntry.save()
                    .then(message.reply(`Updated ${message.author} to: ${characterInfo} ${URL}`))
            } else {

                await CharPics.create({
                    server: serverID,
                    user: userID,
                    picurl: URL2,
                    charinfo: characterInfo,
                });
                message.reply(`Added to CharPics table: ${message.author}: ${characterInfo} ${URL}`);
            }


        })();  //use to close the async if you're using it.
    },
};