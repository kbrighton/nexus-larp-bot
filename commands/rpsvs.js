module.exports = {
    name: 'rpsvs',
    description: 'Run a skill vs. skill RPS test',
    args: true,
    gmonly: false,
    aliases: ['rspvs'],
    usage: '# (Number of your skill level) <name> (Name of opponent or "Difficulty") # (Opposing skill level)',

    execute(message, args) {

        var dt = require('../mymodules');

        var AName = message.author;
        var ANumber = parseInt(args.shift());  //Since this is an array, we can just split like that
        var BNumber = parseInt(args.pop());
        var BName = args.join(" ")


        if (isNaN(ANumber) || isNaN(BNumber)) {
            let reply = `I didn\'t understand that, ${message.author}!`
            reply += '\nThe proper usage would be: \`!rpsVS # (Number of your skill level) <name> (Name of opponent or "Difficulty") # (Opposing skill level)\`';
            message.channel.send(reply);
            return
        }

        var matchNo = 0;
        var Mwin = 'T';
        var AllWin = 'F';
        var CritF = false;

        var MatchString = `RPS Match: ${message.author} at a ${ANumber} vs ${BName} at a ${BNumber} = ${message.author} `;
        var MatchType = `RPS`;

    //     if (PlayKCD) {
    //         MatchString = `KCD Match: ${message.author} at a ${ANumber} vs ${BName} at a ${BNumber} = Remember, K-9 bites Cow, Deputy tases K-9, Cow kicks Deputy.
	// ${message.author} `;
    //         MatchType = `KCD`;
    //     }

        if (ANumber === BNumber) {
            message.channel.send(MatchString + `must win one throw outright to win the match`)
            do {
                matchNo = matchNo + 1;
                Mwin = dt.OneMatch(message, AName, BName, matchNo, true, false);
            } while (Mwin == 'T');
            if (Mwin == 'A') {
                AllWin = 'P'
            } else {
                AllWin = 'F'
                if ((Mwin == 'B') & (matchNo == 1)) {
                    AllWin = 'C'
                }
            }
        } else if (ANumber > BNumber) {
            message.channel.send(MatchString + `must win or tie at least one throw out of ${(ANumber - BNumber)} to win the match`)
            AllWin = dt.RpsRoundHigher(message, AName, ANumber, BName, BNumber, true)
        } else {
            message.channel.send(MatchString + `must win all of ${(BNumber - ANumber)} throws to win the match`)
            AllWin = dt.RpsRoundLower(message, AName, ANumber, BName, BNumber, true)
        }

        if (AllWin == 'P') {
            message.reply(`you \`won\` your ${MatchType} Match vs ${BName}!`);
        } else {
            var MSG = ` sorry, you \`lost\` your ${MatchType} Match vs ${BName}.`;
            if (AllWin == 'C') {
                dt.CritFailState(message, MSG)
            } else {
                message.reply(MSG)
            }
        }

    },
};