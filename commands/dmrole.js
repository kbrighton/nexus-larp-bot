module.exports = {
    name: 'dmrole',
    description: 'send DMs to everyone with a particular role!',
    aliases: ['msgrole'],
    args: true,
    gmonly: true,
    usage: '<role> <message>',


    execute(message, args) {


        var dmRole = args[0];
//		console.log('dmRole = ' + dmRole);
        var dmStart = message.content.indexOf(dmRole) + args[0].length + 1;
        var dmMessage = message.content.slice(dmStart);
//		console.log('msg = ' + dmMessage);

        // First we use guild.members.fetch to make sure all role members are cached
        message.guild.members.fetch().then(fetchedMembers => {
            const totalRole = fetchedMembers.filter(member => member.roles.cache.some(role => role.name == dmRole));
            // We now have a collection with all online member objects in the totalRole variable
            //message.channel.send(`There are currently ${totalRole.size} members online in this guild!`);

            for (var value of totalRole.values()) {
                value.user.send(`Private message from ${message.author} : ${dmMessage}`);
            }

        });


    },
};