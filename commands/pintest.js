module.exports = {
    name: 'pintest',  //name of command in lowercase NOTE: SHOULD BE THE SAME AS THE NAME OF THE .js FILE!
    description: 'Bot makes a message, then pins it',

    args: false,
    gmonly: true,  //make true if only GMs should be able to use this command


    execute(message, args) {
        var dt = require('../mymodules');


        message.channel.send(`Hey ${message.author}, I'm going to pin this post`).then(sentMSG => {
            sentMSG.pin();
        });


    },
};