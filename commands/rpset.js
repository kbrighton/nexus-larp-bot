module.exports = {
    name: 'rpset',
    description: 'Run an Excelence Test ',
    args: true,
    gmonly: false,
    aliases: ['rspet'],
    usage: '# (Number of your skill level)',

    execute(message, args) {

        var dt = require('../mymodules');

        var AName = message.author;
        var BName = 'Skill Level';
        var AString = args[0];
        var ANumber = parseInt(AString);
        var post = false;

        if (isNaN(ANumber) || (ANumber > 20)) {
            let reply = `I didn\'t understand that, ${message.author}!`
            reply += '\nThe proper usage would be: \`!rpsET # (Number of your skill level)\`';
            message.channel.send(reply);
            return
        }

        //if (message.member.roles.cache.some(role => role.name === `GMs`) || message.member.roles.cache.some(role => role.name === `AGMs`)){ post = true };

        message.channel.send(`Excelence Test: ${message.author} at a skill of ${ANumber}`);

        var WinLevel = dt.ExcelenceTest(message, AName, BName, ANumber, post);

        if (WinLevel == 0) {
            message.reply('sorry, you got a 0.');
        } else {
            message.reply(`passed an Excelence Test at a ${WinLevel}!`);
        }

    },
};