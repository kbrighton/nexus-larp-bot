//
//This file initiates access to any DataBases set up.  Add new DBs as a const below InitDodge, and include them in module.exports
//

const Sequelize = require('sequelize');

const sequelize = new Sequelize('database', 'username', 'password', {
    host: 'localhost',
    dialect: 'sqlite',
    logging: false,
    storage: 'database.sqlite',
});

const InitDodge = require('./models/InitDodge')(sequelize, Sequelize);
const CharPics = require('./models/CharPics')(sequelize, Sequelize);


module.exports = {InitDodge, CharPics};
