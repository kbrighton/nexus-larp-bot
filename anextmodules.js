function AKCDThrowResult(ANumber, rGroup) {  //Makes a KCD throw instead of a RPS throw
    const {RockEmoji1, PaperEmoji1, ScisEmoji1, RockEmoji2, PaperEmoji2, ScisEmoji2} = require('./global.json');


    switch (ANumber) {
        case 1:
            return "<:Taser:703477471970066513> `Deputy`"
            break;
        case 2:
            return "<:Cow:703477425111171174> `Cow`"
            break;
        case 3:
            return "<:K9:703477449761095702> `K-9`"
            break;
    }

}

exports.KCDThrowResult = function (ANumber, rGroup) {
    return KCDThrowResult(ANumber, rGroup)
}

function UpdateSwearCount(CountArray, SwearArray, Update) {  //Spazzy Swear Jar
    var fs = require("fs");
    var text = fs.readFileSync("./SwearCount.txt", "utf-8");
    var CurrentArrayUF = text.split("\r\n");
    var CurrentArray = CurrentArrayUF.filter(Boolean);

    if (Update) {  //Update the file with the new numbers
        for (var x in CountArray) {
            if (CountArray[x] > 0) {
                var ANumber = parseInt(CurrentArray[x]);
                ANumber = ANumber + CountArray[x];
                CurrentArray[x] = `${ANumber}`;
            }
        }


        //console.log(CurrentArray);

        const writeStream = fs.createWriteStream('SwearCount.txt');
        const pathName = writeStream.path;
        // write each value of the array on the file breaking line
        CurrentArray.forEach(value => writeStream.write(`${value}\r\n`));
        // the finish event is emitted when all data has been flushed from the stream
        writeStream.on('finish', () => {
            console.log(`wrote all the array data to file ${pathName}`);
        });
        // handle the errors on the write process
        writeStream.on('error', (err) => {
            console.error(`There is an error writing the file ${pathName} => ${err}`)
        });
        // close the stream
        writeStream.end();
    } else {  //If not updating, just print the current count list
        var PrintCount = ``;
        for (var x in CurrentArray) {
            if (x > 0) {
                PrintCount = PrintCount + ` == `
            }
            PrintCount = PrintCount + `${SwearArray[x]}: ${CurrentArray[x]}`;
        }
        return PrintCount;
    }
}


exports.SpazzySwearJar = function (message, SpazzyID, OnlyPrint) {
    const Swears = [/\w*shit\w*/, /\w*piss\w*/, /\w*fuck\w*/, /\w*cunt\w*/, /\w*co+ck\w*/, /\w*tit\w*/, /\w*ass\w*/, /\w*bitch\w*/, /\w*dick\w*/, /\w*damn\w*/, /\w*damm\w*/];
    const SwearArray = ["shit", "piss", "fuck", "cunt", "cock", "tit", "ass", "bitch", "dick", "damn", "damm"];
    const CountArray = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    //console.log(`SJ: ${message.author.username} : ${message}`);

    if (OnlyPrint) {
        return message.channel.send(UpdateSwearCount(CountArray, SwearArray, false));
    }

    var msgLC = message.toString().toLowerCase();
    var msg = msgLC.split(/ +/);
    var hasSwear = false;
    var BNumber = 0;
    const data = [];

    for (var y in msg) {
        for (var x in Swears) {
            var whathesaid = msg[y].match(Swears[x]);
            if (whathesaid) {
                hasSwear = true;
                BNumber = BNumber + 0.25;

                CountArray[x] = CountArray[x] + 1;
                data.push(`Spazzy said "${whathesaid[0]}" (${SwearArray[x]}), add $0.25 to the Swear Jar `);
            }
        }
    }

    if (hasSwear) {

        var dummy = UpdateSwearCount(CountArray, SwearArray, true);

        var fs = require("fs");
        var text = fs.readFileSync("./SwearJar.txt", "utf-8");
        var ANumber = parseFloat(text);

        BNumber = BNumber + ANumber;

        data.push(`The <@${SpazzyID}> Swear Jar is now at $${BNumber.toFixed(2)}`);
        const channel = message.guild.channels.cache.get('694987224625709172');
        channel.send(data);
        //console.log(CountArray);

        const writeStream = fs.createWriteStream('SwearJar.txt');
        const pathName = writeStream.path;
        // write each value of the array on the file breaking line
        writeStream.write(`${BNumber}`);
        // the finish event is emitted when all data has been flushed from the stream
        writeStream.on('finish', () => {
            console.log(`wrote swear jar to file ${pathName}`);
        });
        // handle the errors on the write process
        writeStream.on('error', (err) => {
            console.error(`There is an error writing the file ${pathName} => ${err}`)
        });
        // close the stream
        writeStream.end();
    }
}