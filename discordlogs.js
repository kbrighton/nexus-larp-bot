//
//
//
// ALL CODE RELATED TO LOGGING IN ANY WAY GOES IN THIS FILE
// Before you start, get your Server's ID, and add it as a global variable in index.js


const winston = require('winston');
const fs = require('fs');
const {transports, createLogger, format} = require('winston');
var moment = require('moment');

var an = require('./anextmodules');

const myLevels = {  // Levels are used for logging in some way I could never quite understand
//If you would like everything typed in your sever to be logged to a file, add your LARP name to the bottom with a larger number
//Then update WSoptions below
    values: {
        error: 0,
        warn: 1,
        info: 2,
        verbose: 3,
        debug: 4,
        silly: 5,
        AnimeNextLARP: 10,
        RichterCa: 20
    }
};

var WSoptions = {  // Sets the file name for different logs
//to add a log copy the ANLfile section, and change the section name and log name
//Then update  var Chatlogger = winston.createLogger below
    ANLfile: {
        level: 'AnimeNextLARP',
        filename: 'logs/ANextLARP_log.log',
        handleExceptions: true,
        json: true,
        colorize: false,
    },
    RichterCafile: {
        level: 'RichterCa',
        filename: 'logs/RichterCa_log.log',
        handleExceptions: true,
        json: true,
        colorize: false,
    },
    AllOtherFile: {
        level: 'silly',
        filename: 'logs/BotLogs.log',
        maxsize: 5242880, //5MB
        handleExceptions: true,
        json: true,
        colorize: false,
    },
};

var logger = winston.createLogger({  // Logs server connection stats and handshakes to a file, leave this alone.
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.printf(log => `[${log.timestamp}] - [${log.level.toUpperCase()}] - ${log.message}`)
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File(WSoptions.AllOtherFile),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

exports.getLog = logger;  //Allows index.js to grab the logger from this module

var Chatlogger = winston.createLogger({  //Sets up chat logs to file
// To add your LARP, just add a new line under transports - new winston.transprots.file(WSoptions.<section name you added to WSoptions>);
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.printf(log => `[${log.timestamp}] - [${log.level.toUpperCase()}] - ${log.message}`)
    ),
    levels: myLevels.values,
    transports: [
        new winston.transports.Console(),
        new winston.transports.File(WSoptions.ANLfile),
    ],
    exitOnError: false, // do not exit on handled exceptions
});

exports.getChatlog = Chatlogger;

exports.messageLogging = function (message) {  //logs all messages and DMs to the bot

    var TimeStamp = moment(message.createdAt).format("YYYY-MM-DD HH:mm:ss");
    const ANextServerID = '694983561131982849';

    if ((message.guild == null)) {  //log any DM to the Bot into the log file, but not the ANext Logging Channels
        if (message.channel.type === 'dm') {
            if (message.author.username == 'NexusLarpBot') {
                Chatlogger.AnimeNextLARP(`[${TimeStamp}] - ${message.id} - DirectMessage - TO: @${message.channel.recipient.username} - ${message.cleanContent}`);
            } else {
                Chatlogger.AnimeNextLARP(`[${TimeStamp}] - ${message.id} - DirectMessage - FROM: @${message.author.username} - ${message.cleanContent}`);
            }
        }
    } else {
        if (message.guild.id == ANextServerID) {  //only log things in the ANext Server

            //First Check for Spazzy Swear Jar
            const SpazzyID = "311145912837603331";
            if (message.author.id == SpazzyID) { // TESTING: Messages from Spazzy in the ANext Server get checked for Swear jar
                an.SpazzySwearJar(message, SpazzyID, false);
            }

            const LogGroupId = "698554829064372235";
            if (!(message.channel.parentID == LogGroupId)) {  //don't log things typed into one of the Logging Channels
                //console.log(`${message.channel.name} : ${message.channel.parentID}`);
                var LM = `[${TimeStamp}] - ${message.id} - #${message.channel.name} - @${message.author.username} - ${message.cleanContent}`;
                if (message.attachments.size > 0) {
                    LM += ` - ATTACHMENT: ${message.attachments.map(x => x.url).join(" ")}`;
                }

                Chatlogger.AnimeNextLARP(LM);
                ANextInDiscordLogs(message, false);
            }
//		} else if (message.guild.name == 'RichterCa') {
//			Chatlogger.RichterCa(LM);
        }
    }

}

exports.deleteLogging = function (oldMessage) {
    console.log('Deleted: ' + oldMessage.author + ': ' + oldMessage.content);

    var OldTimeStamp = moment(oldMessage.createdAt).format("YYYY-MM-DD HH:mm:ss");
    var NewTimeStamp = moment().format("YYYY-MM-DD HH:mm:ss");

    if ((oldMessage.guild == null)) {
        if (oldMessage.channel.type === 'dm') {
            if (oldMessage.author.username == 'NexusLarpBot') {
                Chatlogger.AnimeNextLARP(`Directmessage - TO: @${oldMessage.channel.recipient.username} - ${oldMessage.cleanContent}`);
            } else {
                Chatlogger.AnimeNextLARP(`[${NewTimeStamp}] - Directmessage - FROM: @${oldMessage.author.username} - DELETED: [${OldTimeStamp}] ${oldMessage.cleanContent}`);
            }
        }
    } else {
        if (oldMessage.guild.id == ANextServerID) {
            var LM = `[${NewTimeStamp}] - #${oldMessage.channel.name} - @${oldMessage.author.username} - DELETED: [${OldTimeStamp}] ${oldMessage.cleanContent}`;
            Chatlogger.AnimeNextLARP(LM);
            ANextInDiscordLogs(oldMessage, true);
//		} else if (update.oldMessage.guild.name == 'RichterCa') {
//			Chatlogger.RichterCa(LM);
        }
    }

}

exports.updateLogging = function (oldMessage, newMessage) {
    console.log('Updated: ' + oldMessage.content + ' TO: ' + newMessage.content);

    var OldTimeStamp = moment(oldMessage.createdAt).format("YYYY-MM-DD HH:mm:ss");
    var NewTimeStamp = moment(newMessage.editedAt).format("YYYY-MM-DD HH:mm:ss");

    if ((oldMessage.guild == null)) {
        if (oldMessage.channel.type === 'dm') {
            if (oldMessage.author.username == 'NexusLarpBot') {
                Chatlogger.AnimeNextLARP(`Directmessage - TO: @${oldMessage.channel.recipient.username} - ${oldMessage.cleanContent}`);
            } else {
                Chatlogger.AnimeNextLARP(`[${NewTimeStamp}] - Directmessage - FROM: @${oldMessage.author.username} - EDIT: [${OldTimeStamp}] ${oldMessage.cleanContent} EDITTO: ${newMessage.cleanContent}`);
            }
        }
    } else {
        if (oldMessage.guild.id == ANextServerID) {
            var LM = `[${NewTimeStamp}] - ${oldMessage.id} - #${oldMessage.channel.name} - @${oldMessage.author.username} - EDIT: [${OldTimeStamp}] ${oldMessage.cleanContent} EDITTO: ${newMessage.cleanContent}`;
            if (newMessage.attachments.size > 0) {
                LM += ` - ATTACHMENT: ${newMessage.attachments.map(x => x.url).join(" ")}`;
            }
//			newMessage.embeds.forEach((embed) => {
//				// add this embed to the database, using embed.description, embed.fields, etc.
//				// if there are no embeds, this code won't run.
//				console.log(`Has Embed c: ${embed.createdAt}`);
//				var TS = moment(embed.createdAt).format("YYYY-MM-DD HH:mm:ss");
//				LM = `[${TS}] - #${oldMessage.channel.name} - @${oldMessage.author.username} - EMBED: ` + embed.url;
//			});

            for (let embed of newMessage.embeds) { // these are some of the properties
                var TS = moment().format("YYYY-MM-DD HH:mm:ss");
                console.log(`
				Title: ${embed.title}
				Author: ${embed.author}
				Description: ${embed.description}
				Created at: ${embed.createdAt}
				`);
                for (let field of embed.fields) {
                    console.log(`
					Field title: ${field.name}
					Field value: ${field.value}
					`);
                }
                LM = `[${TS}] - ${embed.fields[3].value} - ${embed.fields[0].value} - Poni EMBED - EDIT: ${embed.fields[1].value} EDIT TO: ${embed.fields[2].value}`
            }

            Chatlogger.AnimeNextLARP(LM);
//		} else if (update.oldMessage.guild.name == 'RichterCa') {
//			Chatlogger.RichterCa(LM);
        }
    }

}

function ANextInDiscordLogs(message, deleted) {  // Logs messages from ANext server to different text channels on that server.
    //You don't have to go as crazy as I did here, you can just log to a single channel if you wish.  ~Victor
    const hallChannelGroup = '694983561131982850';
    const hallLogChannelID = '711771632066363455';
    const gmChannelGroup = '694984681912795207'
    const GMlogChannelID = '697257055454625882';
    const oogChannelGroup = '694984592926703788';
    const oogLogChannelID = '698555449913507930';
    const igChannelGroup = '694984556540985377';
    const igLogChannelID = '698555577613156352';
    const museumChannelGroup = '711754713741394053';
    const museumLogChannelID = '711772118148186184';
    const adcapChannelGroup = '711751505090314341';
    const adcapLogChannelID = '711772187324842034';
    const undergroundChannelGroup = '711752631512465448';
    const undergroundLogChannelID = '711772282770554940';
    const sevendragonChannelGroup = '711753512450654289';
    const sevendragonLogChannelID = '711772374567223306';
    const trainingChannelGroup = '714313912551473162';
    const trainingLogChannelID = '714314651189641266';

    var logChannel = message.guild.channels.cache.get(GMlogChannelID);
    var moment = require('moment');
    var TimeStamp = moment(message.createdAt).format("YYYY-MM-DD HH:mm:ss");
    var AuthorName = message.author;

    //if (message.author.bot) { return };

    if (message.member.roles.cache.some(role => role.name === `GMs`) || message.member.roles.cache.some(role => role.name === `AGMs`)) {
        AuthorName = 'GM:' + message.author.username
    }

    var msg = `[${TimeStamp}] ${message.channel}: ${AuthorName}: ${message.cleanContent}`;
    if (deleted) {
        msg += ` - DELETED`;
    }


    switch (message.channel.parentID) {
        case hallChannelGroup:
            logChannel = message.guild.channels.cache.get(hallLogChannelID);
            break;
        case gmChannelGroup:
        default:
            logChannel = message.guild.channels.cache.get(GMlogChannelID);
            break;
        case oogChannelGroup:
            logChannel = message.guild.channels.cache.get(oogLogChannelID);
            break;
        case igChannelGroup:
            logChannel = message.guild.channels.cache.get(igLogChannelID);
            break;
        case museumChannelGroup:
            logChannel = message.guild.channels.cache.get(museumLogChannelID);
            break;
        case adcapChannelGroup:
            logChannel = message.guild.channels.cache.get(adcapLogChannelID);
            break;
        case undergroundChannelGroup:
            logChannel = message.guild.channels.cache.get(undergroundLogChannelID);
            break;
        case sevendragonChannelGroup:
            logChannel = message.guild.channels.cache.get(sevendragonLogChannelID);
            break;
        case trainingChannelGroup:
            logChannel = message.guild.channels.cache.get(trainingLogChannelID);
            break;
    }
    logChannel.send(msg);
}



