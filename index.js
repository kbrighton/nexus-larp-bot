const {Client, GatewayIntentBits, Partials, Collection} = require('discord.js');
const fs = require('fs');
const winston = require('winston');
const {prefix, token} = require('./config.json');
const {InitDodge} = require('./dbObjects');
const {Op} = require('sequelize');
const dt = require('./mymodules');
const dl = require('./discordlogs');
const dln = require('./discordlogsnew.js');
const an = require('./anextmodules');
const ls = require('./loggingsetup');

const bot = new Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.MessageContent,
        GatewayIntentBits.GuildMembers,

    ]
});

// Set up Dynamic commands
bot.commands = new Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    bot.commands.set(command.name, command);
}

bot.once('ready', () => {
    console.log('Ready!');
    ls.readLogServersFile();
    bot.user.setActivity('Rock Paper Scissors', {type: 'PLAYING'});
});

bot.on('ready', () => dl.getLog.log('info', 'The bot is online!'));
bot.on('debug', m => dl.getLog.log('debug', m));
bot.on('warn', m => dl.getLog.log('warn', m));
bot.on('error', m => dl.getLog.log('error', m));

process.on('uncaughtException', error => dl.getLog.log('error', error));

bot.login(token);

bot.on('guildMemberAdd', guildMember => {
    console.log(`Added member: ${guildMember.user.tag}`);
    const Welcome = dt.WelcomeMessage();
    guildMember.send(Welcome)
        .then(message => console.log('Sent welcome message.'))
        .catch(console.error);
});

bot.on('messageReactionAdd', (reaction, user) => {
    // Reaction handling code here
});

bot.on('messageUpdate', async (oldMessage, newMessage) => {
    let tempArray = ls.isLogGuild(oldMessage.guild.id);
    if (tempArray !== -1 && tempArray[3] === "1" && !oldMessage.author.bot && !oldMessage.content.startsWith(prefix)) {
        dln.editLogging(newMessage, oldMessage, tempArray);
    }
});

bot.on('messageDelete', async (oldMessage) => {
    let tempArray = ls.isLogGuild(oldMessage.guild.id);
    if (tempArray !== -1 && tempArray[3] === "1" && !oldMessage.author.bot && !oldMessage.content.startsWith(prefix)) {
        dln.deleteLogging(oldMessage, tempArray);
    }
});

bot.on('messageCreate', message => {
    dl.messageLogging(message);
    if (!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).split(/ +/);
    const commandName = args.shift().toLowerCase();

    const command = bot.commands.get(commandName) ||
        bot.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
    if (!command) return;

    if (command.args && !args.length) {
        let reply = `You didn't provide any arguments, ${message.author}!`;
        if (command.usage) {
            reply += `\nThe proper usage would be: \`${prefix}${commandName} ${command.usage}\``;
        }
        return message.channel.send(reply);
    }

    if (command.gmonly && !(message.member.roles.cache.some(role => role.name === `GMs`) ||
        message.member.roles.cache.some(role => role.name === `AGMs`))) {
        return message.channel.send(`I'm sorry ${message.author}, I can't let you do that.`);
    }

    try {
        command.execute(message, args);
    } catch (error) {
        console.error(error);
        message.reply('there was an error trying to execute that command!');
    }
});