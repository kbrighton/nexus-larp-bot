//
//
//
// ALL CODE RELATED TO LOGGING IN ANY WAY GOES IN THIS FILE

const Discord = require('discord.js');
const winston = require('winston');
const fs = require('fs');
const {transports, createLogger, format} = require('winston');
var moment = require('moment');
/*
exports.loggingSetup = function () {
    LoggingServersInfo.forEach(server => {
        let tempArray = i.split(',');


            let tempTransportName = 'log' + tempArray[0];

            const tempTransportName.toString() = winston.createLogger({
                transports: [
                    new winston.transports.File({
                        filename: 'logs/' + tempTransportName + '.log',
                        level: 'silly',
                    })
                ]
            });


    })
};*/

exports.editLogging = function (newMessage, oldMessage, logArray) {
    const logChannel = oldMessage.client.channels.cache.get(logArray[4]);
    console.log(`In EditLogging, logChannel: ${logChannel}`)
    const editEmbed = new Discord.MessageEmbed;
    editEmbed
        .setTitle(`Edit to post`)
        .setDescription(`A post was edited in ${oldMessage.channel}`)
        .addFields(
            {name: `Message Author:`, value: oldMessage.author},
            {
                name: `Old Message:`,
                value: `${moment(oldMessage.createdAt).format("YYYY-MM-DD HH:mm:ss")}:\n${oldMessage.cleanContent}`
            },
            {
                name: `New Message:`,
                value: `${moment(newMessage.editedAt).format("YYYY-MM-DD HH:mm:ss")}:\n${newMessage.cleanContent}`
            },
            {name: `MessageID:`, value: oldMessage.id}
        )

    logChannel.send(editEmbed)
};

exports.deleteLogging = function (message, logArray) {
    const logChannel = message.client.channels.cache.get(logArray[4]);
    console.log(`In DeleteLogging, logChannel: ${logChannel}`)
    const deleteEmbed = new Discord.MessageEmbed;
    deleteEmbed
        .setTitle(`A post was deleted:`)
        .setDescription(`A post was deleted from ${message.channel}`)
        .addFields(
            {name: `Message Author:`, value: message.author},
            {
                name: `Old Message:`,
                value: `${moment(message.createdAt).format("YYYY-MM-DD HH:mm:ss")}:\n${message.cleanContent}`
            },
            {name: `MessageID:`, value: message.id}
        )

    logChannel.send(deleteEmbed)
}