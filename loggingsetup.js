/*
FOR REFERENCE:
A single guild's logging array consists of:
Guild ID #, Guild Name, File Logging (0/1), Channel Logging (0/1), Logging Channel ID

LoggingServersInfo consists of an array of single guild logging arrays.

LoggingServersIDs is an array of just the GuildIDs from LoggingServersInfo, for easy searching purposes.
*/

function ALogGuild(guildID)         //If this guild is set up for logging, returns an array of Logging info
                                    //If this guild is NOT set up for logging, returns -1
{                   //If this guild is set up for logging, returns an array of Logging info
    let tempArray = [];                         //If this guild is NOT set up for logging, returns -1
    let num = LoggingServersIDs.indexOf(guildID);
    //console.log(`Num is: ${num}`)
    if (num === -1) {
        return num
    } else {
        tempArray = LoggingServersInfo[num].split(',')
        //console.log(`isGuild info: ${tempArray}`)
        return tempArray
    }
}

exports.isLogGuild = function (guildID) {       //If this guild is set up for logging, returns an array of Logging info
    return ALogGuild(guildID);
};


function writeLogIDsArray()                //re-writes the LoggingServerIDs array for easier checking of guild IDs
{
    LoggingServersIDs = [];
    let tempArray = [];
    LoggingServersInfo.forEach(i => {
        tempArray = i.split(',');
        LoggingServersIDs.push(tempArray[0])
    })
    console.log(`Logging IDs: ${LoggingServersIDs}`)
}


exports.readLogServersFile = function ()    //Reads the LoggingServers.txt, and stores it in a Matrix LoggingServersInfo
{

    //This reads the LoggingServers file and parses it into a matrix stored globally.
    var fs = require("fs");
    var text = fs.readFileSync("./LoggingServers.txt", "utf-8");
    var ArrayUF = text.split("\r\n");
    LoggingServersInfo = ArrayUF.filter(Boolean);
    writeLogIDsArray();
};

exports.writeLogServersFile = function ()   //Writes the current LoggingServersInfo matrix into LoggingServers.txt
{

    //Everything below this line writes the data back to the file.
    let fs = require("fs");
    const writeStream = fs.createWriteStream('LoggingServers.txt');

    const pathName = writeStream.path;

    // write each value of the array on the file breaking line
    LoggingServersInfo.forEach(value => writeStream.write(`${value}\r\n`));

    // the finish event is emitted when all data has been flushed from the stream
    writeStream.on('finish', () => {
        console.log(`wrote all the array data to file ${pathName}`);
    });

    // handle the errors on the write process
    writeStream.on('error', (err) => {
        console.error(`There is an error writing the file ${pathName} => ${err}`)
    });

    // close the stream
    writeStream.end();
};

function newChannelSetup(message, emb)      //Confirms setup for Channel logging, and then sets up Channel logging
{
    const Discord = require('discord.js');

    const logEmbed = new Discord.MessageEmbed()
        .setColor('#ffff00')
        .setTitle(`Would you like to set up logging for this server: ${message.guild}?`)
        .setDescription(`Please confirm that you would like to log all changes to and deletions of posts into a channel on this server.`)
        .addField(`Please choose a channel for me to log to`, `Type the channel's name using #\nOr type 'Cancel' to cancel`)

    emb.edit(logEmbed)

    const filter = (reaction, user) => {
        return user.id === message.author.id;
    };
}

function newFileSetup(message, emb, both)   //Sets up File logging for this server. Both is true if user wants to also set up Channel Logging.
{
    const Discord = require('discord.js');
    let emojiPicked = '✅'
    const logEmbed = new Discord.MessageEmbed()
        .setColor('#ffff00')
        .setTitle(`Would you like to set up logging for this server: ${message.guild}?`)
        .setDescription(`Please confirm that you would like every post made in this server logged to a text file.`)
        .addField(`All log text files are stored on my server.`, `If you need to access your log file, please contact Nick @<143178728564850691>`)
        .addField(`Please choose an emoji below`, `📁✅ Confirm\n❌ to Cancel`)

    emb.edit(logEmbed)
    emb.react('✅');
    emb.react('❌');

    const filter = (reaction, user) => {
        return user.id === message.author.id;
    };

    emb.awaitReactions(filter, {max: 1, time: 60000, errors: ['time']})
        .then(collected => {
            emojiPicked = collected.first().emoji
            emb.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
            //message.reply(`emoji chosen: ${emojiPicked}`)
            switch (emojiPicked.name) {
                case "✅":
                    //Do whatever to add file logging
                    //If doing both setups, run new Channel setup, otherwise go to Exists Embed so they can modify or end
                    if (both) {
                        newChannelSetup(message, emb)
                    } else {
                        aExistsEmbed(message, emb)
                    }
                    break;
                case "❌":
                    //Run "Are you sure you want to close" function
                    break;
                default:
                    message.reply('default')
                    break;
            }
        })
        .catch(collected => {
            emb.delete()
            return message.reply(`Sorry, you didn't respond quick enough, and I timed out.  Please try again.`)
        })
}

function newSetup(message, emb)         //Runs when a new guild chooses to set up logging. Choose File, Channel or both.
{
    const Discord = require('discord.js');
    let emojiPicked = '✅'
    const logEmbed = new Discord.MessageEmbed()
        .setColor('#ffff00')
        .setTitle(`Would you like to set up logging for this server: ${message.guild}?`)
        .setDescription(`I can log every post to a text file, log every edit and deletion to a channel on this server, or both.`)
        .addField(`Please choose an emoji below`, `📁 for Log all posts to a Text File\n#️⃣ for Log edits/deletes to a Channel\n✅ for Both\n❌ to Cancel`)

    emb.edit(logEmbed)
    emb.react('📁');
    emb.react('#️⃣');
    emb.react('✅');
    emb.react('❌');

    const filter = (reaction, user) => {
        return user.id === message.author.id;
    };

    emb.awaitReactions(filter, {max: 1, time: 60000, errors: ['time']})
        .then(collected => {
            emojiPicked = collected.first().emoji
            emb.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
            //message.reply(`emoji chosen: ${emojiPicked}`)
            switch (emojiPicked.name) {
                case '📁':
                    //Run New File function
                    message.reply(`You picked 📁`)
                    newFileSetup(message, emb, false)
                    break;
                case '#️⃣':
                    //Run New Channel function
                    message.reply(`You picked #️⃣`)
                    break;
                case "✅":
                    //Run New File then New Channel functions
                    newFileSetup(message, emb, true)
                    break;
                case "❌":
                    //Run "Are you sure you want to close" function
                    break;
                default:
                    message.reply('default')
                    break;
            }
        })
        .catch(collected => {
            emb.delete()
            return message.reply(`Sorry, you didn't respond quick enough, and I timed out.  Please try again.`)
        })
}

exports.firstEmbed = function (message, emb)        //The first Embed that runs if a guild is not set up for logging yet.
{
    const Discord = require('discord.js');
    const logEmbed = new Discord.MessageEmbed()
        .setTitle(`Would you like to set up logging for this server: ${message.guild}?`)
        .setDescription(`I can log every post to a text file, log every change and deletion to a channel on this server, or both.`)
        .addField(`Please choose an emoji below to continue with logging setup`, `✅ for Yes\n❌ for No`)
    emojiPicked = '✅'

    emb.edit(logEmbed);
    emb.react('✅');
    emb.react('❌');

    const filter = (reaction, user) => {
        return user.id === message.author.id;
    };

    emb.awaitReactions(filter, {max: 1, time: 60000, errors: ['time']})
        .then(collected => {
            //console.log(`collected: ${collected.first().emoji}`)
            emojiPicked = collected.first().emoji
            emb.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
            if (emojiPicked.name === '✅') {
                newSetup(message, emb);
            } else {
                logEmbed
                    .setDescription(`Logging setup canceled.`)
                    .setColor("#ff0000")
                    .fields = []
                return emb.edit(logEmbed)
            }
        })
        .catch(collected => {
            //console.log(`After a minute, only ${collected.size} out of 4 reacted.`);
            emb.delete()
            message.reply(`Sorry, you didn't respond quick enough, and I timed out.  Please try again.`)
        })
};

function aExistsEmbed(message, emb)        //The embed that runs if a guild with logging already set up runs the logging command
{
    const Discord = require('discord.js');
    const logEmbed = new Discord.MessageEmbed()
        .setTitle(`Logging for server: ${message.guild}`)
        .setColor("#ffff00")

    let tempArray = ALogGuild(message.guild.id);
    let isLogToFile = "No", isLogToChannel = "No", logChannelID = 0;
    if (tempArray[2] === "1") {
        isLogToFile = "Yes"
    }
    if (tempArray[3] === "1") {
        logChannelID = tempArray[4]
        console.log(`in logging: modify: ${logChannelID}`)
        let logChannel = message.guild.channels.cache.get(logChannelID)
        isLogToChannel = `Yes: ${logChannel}`
    }

    logEmbed.setDescription(`Logging is already set up for this server, with the following information:`)
        .addField(`Logging to a file:`, `${isLogToFile}`)
        .addField(`Logging changes/deletions to a channel:`, `${isLogToChannel}`)
        .addField(`Would you like to modify or delete your setup? Please choose an emoji below`, `⚙️ to Modify\n🚫 to Delete\n❌ to Cancel`)
    emojiPicked = '✅'

    emb.edit(logEmbed);
    emb.react('⚙️');
    emb.react('🚫');
    emb.react('❌');

    const filter = (reaction, user) => {
        return user.id === message.author.id;
    };

    emb.awaitReactions(filter, {max: 1, time: 60000, errors: ['time']})
        .then(collected => {
            //console.log(`collected: ${collected.first().emoji}`)
            emojiPicked = collected.first().emoji
            //message.reply(`you picked ${emojiPicked}`)
            emb.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
            switch (emojiPicked.name) {
                case "⚙️":
                    //Run Modify function
                    message.reply(`You picked ⚙️`)
                    break;
                case '🚫':
                    //Run Delete function
                    message.reply(`You picked 🚫`)
                    break;
                case "❌":
                    //Run "Are you sure you want to close" function
                    break;
            }
        })
        .catch(collected => {
            //console.log(`After a minute, only ${collected.size} out of 4 reacted.`);
            emb.delete()
            message.reply(`Sorry, you didn't respond quick enough, and I timed out.  Please try again.`)
        })

}

exports.existsEmbed = function (message, emb) {
    aExistsEmbed(message, emb)
};